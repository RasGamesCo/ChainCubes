﻿using UnityEditor;


namespace EyePM
{
    [CustomEditor(typeof(UIGemParticleManager))]
    public class UIGemParticleManagerEditor : UIResourceParticleManagerEditor<UIGemParticleManager>
    {

    }
}
