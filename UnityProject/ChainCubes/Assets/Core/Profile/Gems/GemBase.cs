﻿//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////
using UnityEngine;

public class GemBase : MonoBehaviour
{
    protected virtual void Awake()
    {
        PlayModel.stats.totalGems++;
    }
}
