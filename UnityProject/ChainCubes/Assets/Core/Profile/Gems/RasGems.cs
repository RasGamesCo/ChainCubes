﻿//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////
using UnityEngine;
using EyePM;

namespace Ras.Profile
{
    public partial class Data
    {
        public CryptoInt gems = 0;
    }
}


public static partial class Profile
{
    public static int Gems => data.gems;

    public static void EarnGems(int value)
    {
        if (value <= 0) return;
        data.gems = data.gems.Value + value;
    }

    public static bool SpendGems(int value)
    {
        if (Gems >= value)
        {
            data.gems = data.gems.Value - value;
            return true;
        }
        return false;
    }
}


public static partial class PhysicsLayer
{
    public static int Gem = LayerMask.NameToLayer("Gem");
}


public partial class RasFactory : StaticConfig<RasFactory>
{
    public static class Gems
    {
        public static GemBase Create(Transform parent)
        {
            var prefab = Resources.Load<GemBase>("Game/Prefabs/Gem");
            prefab.gameObject.layer = PhysicsLayer.Gem;
            return prefab != null ? prefab.Clone<GemBase>(parent, false, true) : null;
        }

        public static T Create<T>(Transform parent) where T : Component
        {
            var prefab = Resources.Load<T>("Game/Prefabs/Gem");
            return prefab?.Clone<T>(parent, false, true);
        }
    }
}

public static partial class RasAnalytics
{
    // A “sink” is when a player loses or spends a resource
    public static void SinkGem(string gate, int amount, string itemId)
    {
        Resources.Sink("gem", gate, itemId, amount);
    }

    // A “source” is when a player gains or earns a resource
    public static void SourceGem(string gate, int amount, string itemId)
    {
        Resources.Source("gem", gate, itemId, amount);
    }
}



public static partial class PlayModel
{
    public partial class Stats
    {
        public int totalGems = 0;
        public int grabGems = 0;
    }
}

namespace Ras.Config
{
    public partial class ProfilePreset
    {
        public int gems = 0;
    }

    public partial class RewardData
    {
        public partial class Pack
        {
            [Tooltip("x:Chance y:Min, z:Max")]
            public Vector3Int gems;
        }
    }
}
