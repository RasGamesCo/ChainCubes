﻿//////////////////////////////////////////////////////

/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////
public partial class LevelModel
{
}

public partial class RewardModel
{
}

public static partial class PlayModel
{
    public partial class Stats
    {
    }

    public static Stats stats = new Stats();
    public static LevelModel level = new LevelModel();
    public static RewardModel reward = new RewardModel();

    public static System.Action<System.Action> onLeaveRequest = null;
    public static System.Action<System.Action<bool>> onLoosing = null;
    public static System.Action<System.Action> onLosed = null;
    public static System.Action<System.Action> onWin = null;
}
