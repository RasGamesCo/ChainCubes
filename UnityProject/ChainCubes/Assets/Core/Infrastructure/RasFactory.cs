﻿//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////

using EyePM;
using UnityEngine;


//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////
public partial class RasFactory : StaticConfig<RasFactory>
{
    [System.Serializable]
    public partial class SoundEffects
    {
        public AudioClip buttonClick = null;
        public AudioClip start = null;
        public AudioClip win = null;
        public AudioClip lose = null;
    }

    [SerializeField] private SoundEffects soundEffects = new SoundEffects();

    //////////////////////////////////////////////////////
    /// STATIC MEMBERS
    /// RasGames infrastructure class
    /// !! DO NOT CHANGE THIS FILE !!
    //////////////////////////////////////////////////////
    public static SoundEffects Sounds => Instance.soundEffects;

    #if UNITY_EDITOR
    [UnityEditor.MenuItem("EyePM/Factory", priority = 0)]
    private static void SelectMe()
    {
        UnityEditor.Selection.activeObject = Instance;
    }
    #endif
}
