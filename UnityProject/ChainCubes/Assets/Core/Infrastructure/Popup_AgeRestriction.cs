﻿using UnityEngine;
using UnityEngine.UI;
using EyePM;

public class Popup_AgeRestriction : GameState
{
    [SerializeField] private Text title = null;
    [SerializeField] private Toggle acceptToggle = null;
    [SerializeField] private Button agreeButton = null;
    [SerializeField] private Button privacePolicyButton = null;

    void Start()
    {
        title.text = Application.productName + " GDRP";
        agreeButton.SetInteractable(acceptToggle.isOn = false);

        acceptToggle.onValueChanged.AddListener(isOn => agreeButton.SetInteractable(isOn));
        privacePolicyButton.onClick.AddListener(() => Application.OpenURL(RasConfig.Socials.privacyPolicyUrl));

        agreeButton.onClick.AddListener(() =>
        {
            base.Back();
            AgeRestriction.Accepted = true;
            AgeRestriction.OnAccept?.Invoke();
        });

        UIShowHide.ShowAll(transform);
    }

    public override void Back()
    {

    }
}


public static class AgeRestriction
{
    [System.Serializable]
    public class IpInfo
    {
        public string country = string.Empty;
        public string timezone = string.Empty;
    }

    public static System.Action OnAccept = null;

    public static bool NeedData => Enabled && Accepted == false && (Countries.IsNullOrEmpty() || LocationJson.IsNullOrEmpty());

    private static bool Enabled
    {
        get => PlayerPrefs.GetInt("AgeRestriction.Enabled", 1) > 0;
        set => PlayerPrefs.SetInt("AgeRestriction.Enabled", value ? 1 : 0);
    }

    public static bool Accepted
    {
        get => PlayerPrefs.GetInt("AgeRestriction.Accept", 0) > 0;
        set => PlayerPrefs.SetInt("AgeRestriction.Accept", value ? 1 : 0);
    }

    public static string Countries
    {
        get => PlayerPrefs.GetString("AgeRestriction.Countries", string.Empty);
        set => PlayerPrefs.SetString("AgeRestriction.Countries", value.HasContent(1) ? value : Countries);
    }

    public static string LocationJson
    {
        get => PlayerPrefs.GetString("AgeRestriction.LocaltionJson", string.Empty);
        set => PlayerPrefs.SetString("AgeRestriction.LocaltionJson", value.HasContent(10) ? value : LocationJson);
    }

    public static void Check(System.Action onNextTask)
    {
        if (Accepted || Enabled == false || Countries.IsNullOrEmpty() || LocationJson.IsNullOrEmpty())
        {
            onNextTask?.Invoke();
            return;
        }

        try
        {
            var ipInfo = JsonUtility.FromJson<IpInfo>(LocationJson);
            if (ipInfo.country.IsNullOrEmpty() && ipInfo.timezone.IsNullOrEmpty())
            {
                onNextTask?.Invoke();
                return;
            }
            ipInfo.country = ipInfo.country?.ToLower();
            ipInfo.timezone = ipInfo.timezone?.ToLower();

            var countryList = Countries.ToLower().Split(new string[] { " ", ";", "\r\n", "\n\r" }, System.StringSplitOptions.RemoveEmptyEntries);
            if (countryList.Length > 0)
            {
                Enabled = false;
                foreach (var country in countryList)
                {
                    if (ipInfo.country == country || ipInfo.timezone.Contains(country))
                    {
                        Enabled = true;
                        break;
                    }
                }
            }
        }
        catch (System.Exception error)
        {
            Debug.LogError(error.Message);
            onNextTask?.Invoke();
        }


        if (Enabled)
        {
            OnAccept = onNextTask;
            // Game.Instance.OpenPopup<Popup_AgeRestriction>();
        }
    }

    public static void Withdraw()
    {
        Accepted = false;
        // Game.Instance.OpenPopup<Popup_AgeRestriction>();
    }
}