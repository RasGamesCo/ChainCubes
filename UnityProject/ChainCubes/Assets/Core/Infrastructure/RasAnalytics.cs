﻿//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////

#if GASDK
using GameAnalyticsSDK;
#endif
using System.Collections.Generic;
using EyePM;
using UnityEngine;


//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////
public static partial class RasAnalytics
{
    [DefaultExecutionOrder(-9999)]
    public class RasAnalyticsMono : MonoBehaviour
    {
        private void Awake()
        {
            #if GASDK
            GameAnalytics.Initialize();
            #endif
        }
    }

    //////////////////////////////////////////////////////
    /// STATIC MEMBERS
    /// RasGames infrastructure class
    /// !! DO NOT CHANGE THIS FILE !!
    /////////////////////////////////////////////////////////
    #if GASDK
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void OnRuntimeInitialized()
    {
        Object.DontDestroyOnLoad(new GameObject("RasAnalytics", typeof(RasAnalyticsMono)));
    }
    #endif
    private static void SetGroup(int index)
    {
        #if GASDK
        GameAnalytics.SetCustomDimension01("group_" + Mathf.Clamp(index, 0, 3));
        #endif
    }

    private static void ValidateCustomeDimension(int index, string value)
    {
        #if GASDK
        List<string> list = null;

        switch (index)
        {
            case 1: list = GameAnalytics.SettingsGA.CustomDimensions01; break;
            case 2: list = GameAnalytics.SettingsGA.CustomDimensions02; break;
            case 3: list = GameAnalytics.SettingsGA.CustomDimensions03; break;
			case 4: list = GameAnalytics.SettingsGA.CustomDimensions03; break;
        }

        if (list.Exists(x => x == value) == false)
            list.Add(value);
        #endif
    }

    public static class ABTest
    {
        public static System.Action OnRecieved = null;

        public static string Info
        {
            get
            {
                #if GASDK
                var id = GameAnalytics.GetABTestingId();
                var group = GameAnalytics.GetABTestingVariantId();
                return string.IsNullOrEmpty(id) ? "None" : ("Id: " + id + " Group: " + group);
                #else
                return string.Empty;
                #endif
            }
        }

        public static string Get(string key, string defaultValue)
        {
            #if GASDK
            if (GameAnalytics.IsRemoteConfigsReady() == false) return defaultValue;
            var res = GameAnalytics.GetRemoteConfigsValueAsString(key, defaultValue);
            Debug.Log("GameAnalytics remote config: " + key + " = " + defaultValue + " ~> " + res);
            return res;
            #else
            return defaultValue;
            #endif
        }

        public static int Get(string key, int defaultValue)
        {
            return Get(key, defaultValue.ToString()).ToInt(defaultValue);
        }

        public static float Get(string key, float defaultValue)
        {
            return Get(key, defaultValue.ToString()).ToFloat(defaultValue);
        }

        public static int GetGroup(int defaultValue)
        {
            int res = Get("group", defaultValue);
            SetGroup(res);
            return res;
        }
    }

    public static class Progression
    {
        public static void Start(long createdCubeValue)
        {
            #if GASDK
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "CubeCreated", $"Cube_{createdCubeValue}", null, 0);
            #endif
        }

        public static void Fail(long bestCreatedCubeValue)
        {
            #if GASDK
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, $"BestCube_{bestCreatedCubeValue}", null, null , 0);
            #endif
        }
    }

    public static class Resources
    {
        // A “sink” is when a player loses or spends a resource
        public static void Sink(string name, string gate, string item, int amount)
        {
            #if GASDK
            Validate(name, gate);
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, name, amount, gate, item);
            #endif
        }

        // A “source” is when a player gains or earns a resource
        public static void Source(string name, string gate, string item, int amount)
        {
            #if GASDK
            Validate(name, gate);
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, name, amount, gate, item);
            #endif
        }

        private static void Validate(string name, string gate)
        {
            #if GASDK
            if (GameAnalytics.SettingsGA.ResourceCurrencies.Exists(x => x == name) == false)
                GameAnalytics.SettingsGA.ResourceCurrencies.Add(name);

            if (GameAnalytics.SettingsGA.ResourceItemTypes.Exists(x => x == gate) == false)
                GameAnalytics.SettingsGA.ResourceItemTypes.Add(gate);
            #endif
        }
    }

    public static class Ad
    {
        public static class Rewarded
        {
            public static void Requested(string placement)
            {
                #if GASDK
                GameAnalytics.NewAdEvent(GAAdAction.Request, GAAdType.RewardedVideo, "tapsell", placement);
                #endif
            }

            public static void RequestSucceed(string placement)
            {
                #if GASDK
                GameAnalytics.NewAdEvent(GAAdAction.Loaded, GAAdType.RewardedVideo, "tapsell", placement);
                #endif
            }

            public static void Failed(string placement)
            {
                #if GASDK
                if (Application.internetReachability != NetworkReachability.NotReachable)
                    GameAnalytics.NewAdEvent(GAAdAction.FailedShow, GAAdType.RewardedVideo, "tapsell", placement);
                #endif
            }

            public static void Showed(string placement)
            {
                #if GASDK
                GameAnalytics.NewAdEvent(GAAdAction.Show, GAAdType.RewardedVideo, "tapsell", placement);
                #endif
            }

            public static void Succeeded(string placement)
            {
                #if GASDK
                GameAnalytics.NewAdEvent(GAAdAction.RewardReceived, GAAdType.RewardedVideo, "tapsell", placement);
                #endif
            }
        }

        public static class Interstitial
        {
            public static void Requested(string placement)
            {
                #if GASDK
                GameAnalytics.NewAdEvent(GAAdAction.Request, GAAdType.Interstitial, "tapsell", placement);
                #endif
            }

            public static void RequestSucceed(string placement)
            {
                #if GASDK
                GameAnalytics.NewAdEvent(GAAdAction.Loaded, GAAdType.Interstitial, "tapsell", placement);
                #endif
            }

            public static void Failed(string placement)
            {
                #if GASDK
                if (Application.internetReachability != NetworkReachability.NotReachable)
                    GameAnalytics.NewAdEvent(GAAdAction.FailedShow, GAAdType.Interstitial, "tapsell", placement);
                #endif
            }

            public static void Showed(string placement)
            {
                #if GASDK
                GameAnalytics.NewAdEvent(GAAdAction.Show, GAAdType.Interstitial, "tapsell", placement);
                #endif
            }
        }

        public static class Banner
        {
            public static void Requested(string placement)
            {
                #if GASDK
                GameAnalytics.NewAdEvent(GAAdAction.Request, GAAdType.Banner, "tapsell", placement);
                #endif
            }

            public static void RequestSucceed(string placement)
            {
                #if GASDK
                GameAnalytics.NewAdEvent(GAAdAction.Loaded, GAAdType.Banner, "tapsell", placement);
                #endif
            }

            public static void Failed(string placement)
            {
                #if GASDK
                if (Application.internetReachability != NetworkReachability.NotReachable)
                    GameAnalytics.NewAdEvent(GAAdAction.FailedShow, GAAdType.Banner, "tapsell", placement);
                #endif
            }

            public static void Showed(string placement)
            {
                #if GASDK
                GameAnalytics.NewAdEvent(GAAdAction.Show, GAAdType.Banner, "tapsell", placement);
                #endif
            }
        }

        public static class Cross
        {
            public static void Show(string name)
            {
                #if GASDK
                GameAnalytics.NewDesignEvent("ad:cross:show:" + name.Replace(" ", string.Empty).ToLower(), 1);
                #endif
            }

            public static void Clicked(string name)
            {
                #if GASDK
                GameAnalytics.NewDesignEvent("ad:cross:clicked:" + name.Replace(" ", string.Empty).ToLower(), 1);
                #endif
            }
        }
    }
    #if METRIX
    public static partial class Metrix
    {
        #if UNITY_ANDROID
        private static AndroidJavaClass metrix = null;
        #endif
        private static void GetJavaObject()
        {
            #if UNITY_ANDROID && (ABR || METRIX)
            if (metrix != null)
                return;
            metrix = new AndroidJavaClass("ir.metrix.sdk.MetrixUnity");
            #endif
        }

        public static void NewEvent(string eventName)
        {
            if (eventName.IsNullOrEmpty()) return;
            if (eventName.Length > 7)
            {
                Debug.LogWarning("Wrong metrix event: " + eventName);
                return;
            }

            #if UNITY_ANDROID
            GetJavaObject();
            metrix?.CallStatic("newEvent", eventName);
            #endif
        }

        public static void NewEvent(string eventName, Dictionary<string, string> customAttributes)
        {
            if (eventName.IsNullOrEmpty()) return;
            if (eventName.Length > 7)
            {
                Debug.LogWarning("Wrong metrix event: " + eventName);
                return;
            }

            #if UNITY_ANDROID
            GetJavaObject();
            metrix?.CallStatic("newEvent", eventName, ConvertDictionaryToMap(customAttributes));
            #endif
        }

        private static AndroidJavaObject ConvertDictionaryToMap(IDictionary<string, string> parameters)
        {
            var javaMap = new AndroidJavaObject("java.util.HashMap");
            System.IntPtr putMethod = AndroidJNIHelper.GetMethodID(javaMap.GetRawClass(), "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
            var args = new object[2];
            foreach (KeyValuePair<string, string> kvp in parameters)
            {
                using (AndroidJavaObject k = new AndroidJavaObject("java.lang.String", kvp.Key))
                {
                    using (AndroidJavaObject v = new AndroidJavaObject("java.lang.String", kvp.Value))
                    {
                        args[0] = k;
                        args[1] = v;
                        AndroidJNI.CallObjectMethod(javaMap.GetRawObject(), putMethod, AndroidJNIHelper.CreateJNIArgArray(args));
                    }
                }
            }
            return javaMap;
        }
    }
    #endif
}