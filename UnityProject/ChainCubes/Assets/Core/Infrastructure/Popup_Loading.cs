﻿using EyePM;
using UnityEngine;
using UnityEngine.UI;


public class Popup_Loading : GameState
{
    [SerializeField] private GameObject defaultObject = null;
    [SerializeField] private GameObject updateObject = null;
    [SerializeField] private Button updateButton = null;

    private void Start()
    {
        updateButton.onClick.AddListener(() =>
        {
            #if PM_SHRNG
            SocialAndSharing.OpenPage();
            #endif
            Application.Quit();
        });

        if (instance != null)
            UIShowHide.ShowAll(transform);
    }

    private void Update()
    {
        if (updateObject)
            updateObject.SetActive(RasConfig.Update.mode == Ras.Config.Update.Mode.Force);
        if (defaultObject)
            defaultObject.SetActive(RasConfig.Update.mode != Ras.Config.Update.Mode.Force);
        transform.SetAsLastSibling();
    }

    public override void Back()
    {
        // ;)
    }

    ///////////////////////////////////////////////////////////////////////////////////
    //  STATIC MEMBERS
    ///////////////////////////////////////////////////////////////////////////////////
    private static Popup_Loading instance = null;
    private static int count = 0;

    public static void Show()
    {
        count++;
        if (instance != null)
            return;
        instance = RasGame.RASGame.OpenPopup<Popup_Loading>();
    }

    public static void Hide()
    {
        if (instance == null)
            return;
        if (RasConfig.Update.mode == Ras.Config.Update.Mode.Force) return;
        count--;
        if (count > 0)
            return;
        RasGame.RASGame.Back(instance);
        instance = null;
    }
}


///////////////////////////////////////////////////////////////////////////////////
//  Helper class
///////////////////////////////////////////////////////////////////////////////////
public static class Loading
{
    public static void Show() { Popup_Loading.Show(); }
    public static void Hide() { Popup_Loading.Hide(); }
}
