﻿//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////

using System;
using System.Collections;
using EyePM;
using UnityEngine;
using Object = UnityEngine.Object;


#if TAPSELL
using TapsellSDK;


#endif

//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////
public static partial class RasAd
{
    [Serializable]
    public partial class Zone
    {
        public enum State
        {
            Idle = 0,
            Requesting = 1,
            Ready = 2
        }

        [Space]
        public string zoneBazaar = string.Empty;
        public string zoneMyKet = string.Empty;
        public string zoneGooglePlay = string.Empty;
        public string zoneTV = string.Empty;

        #if BAZAAR
        public string ZoneId => zoneBazaar;
        #elif MYKET
        public string ZoneId => zoneMyKet;
        #elif GOOGLE
        public string ZoneId => zoneGooglePlay;
        #else
        public string ZoneId => zoneTV;
        #endif

        public State CurrentState { get; set; } = State.Idle;
        public bool IsIdle => CurrentState == State.Idle;
        public bool IsReady => CurrentState == State.Ready;
        public bool IsRequesting => CurrentState == State.Requesting;
        public bool IsOff => ZoneId.IsNullOrEmpty();
    }

    [Serializable]
    public class AdPlace
    {
        public enum Type
        {
            Rewarded = 0,
            Interstitial = 1,
            Banner = 2
        }

        public Type type;
        public string name;
        public int reward;
        public bool[] active;

        public int Index { get; set; } = 0;
        public bool IsActive => active.HasItem() && active[Index % active.Length];

        public AdPlace(Type adType, string placeName, uint activeSize = 1)
        {
            type = adType;
            name = placeName;
            active = new bool[activeSize];
            for (int i = 0; i < activeSize; active[i++] = true) ;
        }
    }

    public class AdMono : MonoBehaviour
    {
        private IEnumerator Start()
        {
            var wait = new WaitForSecondsRealtime(RasConfig.AdZones.requestInterval);
            while (true)
            {
                try
                {
                    Rewarded.Request();
                }
                catch
                {
                    Debug.LogError("Rewarded Request Error");
                }

                yield return wait;

                try
                {
                    Interstitial.Request();
                }
                catch
                {
                    Debug.LogError("Interstitial Request Error");
                }

                yield return wait;
            }
        }

        private void OnApplicationQuit()
        {
            #if UNITY_EDITOR
            Banner.HideBanner();
            #endif
        }
    }

    //////////////////////////////////////////////////////
    /// STATIC MEMBERS
    /// RasGames infrastructure class
    /// !! DO NOT CHANGE THIS FILE !!
    //////////////////////////////////////////////////////
    public static Zone bannerZone = null;
    public static Zone rewardedZone = null;
    public static Zone interstitialZone = null;
    public static Func<AdPlace, bool> onPreOpenConfition = adPlace => true;
    public static event Action<Zone> OnRequested = null;
    public static event Action<Zone> OnRequestSucceed = null;
    public static event Action<Zone, string> OnRequestFailed = null;
    public static event Action<AdPlace> OnOpened = null;
    public static event Action<AdPlace> OnOpenFailed = null;
    public static event Action<AdPlace, bool> OnClosed = null;

    public static void Initialize(Zone banner, Zone rewarded, Zone interstitial)
    {
        #if TAPSELL
        Tapsell.Initialize(RasConfig.ApiKeys.tapsell);
        #elif TAPSELLPLUS
        TapsellPlus.Initialize(RasConfig.ApiKeys.tapsellPlus);
        #elif ADMOB
        AdMob.Initialize(RasConfig.ApiKeys.admob);
        #elif UAD
        UAD.Initialize(RasConfig.ApiKeys.uad);
        #endif

        bannerZone = banner;
        rewardedZone = rewarded;
        interstitialZone = interstitial;
        banner.CurrentState = Zone.State.Idle;
        rewarded.CurrentState = Zone.State.Idle;
        interstitial.CurrentState = Zone.State.Idle;
        Object.DontDestroyOnLoad(new GameObject("RasAd").AddComponent<AdMono>());
    }

    public static void Show(AdPlace place, Action<bool, bool> callback = null) // success and completed, showingStarted
    {
        switch (place.type)
        {
            case AdPlace.Type.Rewarded:
                Rewarded.ShowRewarded(place, callback);
                break;
            case AdPlace.Type.Interstitial:
                Interstitial.ShowInterstitial(place, callback);
                break;
            case AdPlace.Type.Banner:
                Banner.RequestAndShow(place);
                break;
            default: throw new ArgumentOutOfRangeException();
        }
    }

    public static partial class Banner
    {
        public static bool IsReady => bannerZone.IsReady;

        public static void RequestAndShow(AdPlace adPlace)
        {
            if (onPreOpenConfition(adPlace) == false || bannerZone.IsOff || bannerZone.IsRequesting)
                return;

            adPlace.Index++;
            if (bannerZone.IsReady)
            {
                HideBanner();
                RequestAndShow(adPlace);
                if (!adPlace.IsActive)
                    return;
            }

            bannerZone.CurrentState = Zone.State.Requesting;
            OnRequested?.Invoke(bannerZone);

            #if UNITY_EDITOR
            if (Application.isPlaying)
            {
                bannerZone.CurrentState = Zone.State.Ready;
                OnRequestSucceed?.Invoke(bannerZone);
                OnOpened?.Invoke(adPlace);
                return;
            }
            #endif
            #if ADMOB || UAD || TAPSELL || TAPSELLPLUS
            try
            {
                RequestViaSDK(adPlace);
            }
            catch (Exception)
            {
                // ignored
            }
            #endif
        }

        public static void HideBanner()
        {
            if (!bannerZone.IsReady)
                return;
            Debug.Log("______________________________ RasAd.Banner.Hide: " + bannerZone.ZoneId);
            bannerZone.CurrentState = Zone.State.Idle;
            #if UNITY_EDITOR
            return;
            #elif ADMOB || UAD || TAPSELL || TAPSELLPLUS
            HideViaSDK();
            #endif
        }
    }

    public static partial class Rewarded
    {
        public static bool IsReady => rewardedZone.IsReady;

        public static void Request()
        {
            if (rewardedZone.IsOff || rewardedZone.IsIdle == false)
                return;

            rewardedZone.CurrentState = Zone.State.Requesting;
            OnRequested?.Invoke(rewardedZone);

            #if UNITY_EDITOR
            if (Application.isPlaying)
            {
                rewardedZone.CurrentState = Zone.State.Ready;
                OnRequestSucceed?.Invoke(rewardedZone);
                return;
            }
            #endif
            #if TAPSELL || TAPSELLPLUS || ADMOB || UAD
            RequestViaSDK();
            #endif
        }

        public static void ShowRewarded(AdPlace adPlace, Action<bool, bool> callback) // success and completed, showingStarted
        {
            if (onPreOpenConfition(adPlace) == false)
            {
                callback?.Invoke(false, false);
                return;
            }

            if (adPlace.IsActive == false)
            {
                adPlace.Index++;
                callback?.Invoke(false, false);
                return;
            }
            adPlace.Index++;
            ShowAd(rewardedZone, adPlace, callback);
        }
    }

    public static partial class Interstitial
    {
        public static bool IsReady => interstitialZone.IsReady;

        public static void Request()
        {
            if (interstitialZone.IsOff || interstitialZone.IsIdle == false)
                return;
            interstitialZone.CurrentState = Zone.State.Requesting;
            OnRequested?.Invoke(interstitialZone);

            #if UNITY_EDITOR
            if (Application.isPlaying)
            {
                interstitialZone.CurrentState = Zone.State.Ready;
                OnRequestSucceed?.Invoke(interstitialZone);
                return;
            }
            #endif
            #if TAPSELL || TAPSELLPLUS || ADMOB || UAD
            RequestViaSDK();
            #endif
        }

        public static void ShowInterstitial(AdPlace adPlace, Action<bool, bool> callback) // success and completed, showingStarted
        {
            if (onPreOpenConfition(adPlace) == false)
            {
                callback?.Invoke(false, false);
                return;
            }

            if (adPlace.IsActive == false)
            {
                adPlace.Index++;
                callback?.Invoke(false, false);
                return;
            }
            adPlace.Index++;
            ShowAd(interstitialZone, adPlace, callback);
        }
    }

    private static void ShowAd(Zone zone, AdPlace adPlace, Action<bool, bool> callback) // success and completed, showingStarted
    {
        if (zone.IsReady == false)
        {
            OnOpenFailed?.Invoke(adPlace);
            callback?.Invoke(false, false);
            return;
        }
        #if UNITY_EDITOR
        if (Application.isPlaying)
        {
            if(RasConfig.EditorData.rewardedShowingStartedInEditor)
                OnOpened?.Invoke(adPlace);
            zone.CurrentState = Zone.State.Idle;
            if(RasConfig.EditorData.rewardedShowingStartedInEditor)
                OnClosed?.Invoke(adPlace, true);
            callback?.Invoke(RasConfig.EditorData.rewardedShowingSuccessedInEditor, RasConfig.EditorData.rewardedShowingStartedInEditor);
            return;
        }
        #endif
        #if TAPSELL || TAPSELLPLUS || ADMOB || UAD
        ShowAdViaSDK(zone, adPlace, callback);
        #endif
    }
}