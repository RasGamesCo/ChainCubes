﻿//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////

using UnityEngine;
using Ras.Profile;
using EyePM;
using System.Collections.Generic;


namespace Ras.Profile
{
    [System.Serializable]
    public partial class Item
    {
        public string sku = string.Empty;
        public long time = 0;
    }

    [System.Serializable]
    public partial class Data
    {
        public CryptoInt sessions = 0;
        public CryptoInt level = 1;

        public List<Item> items = new List<Item>();
    }
}


//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////
public static partial class Profile
{
    public class ProfileMono : MonoBehaviour
    {

        private void Awake()
        {
            LoadLocal();
            data.sessions++;
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
                SaveLocal();
        }

        private void OnApplicationQuit()
        {
            SaveLocal();
        }
    }

    ////////////////////////////////////////////
    /// STATIC MEMBERS
    /// RasGames infrastructure class
    /// !! DO NOT CHANGE THIS FILE !!
    ////////////////////////////////////////////
    private static Data data = new Data();

    public static bool IsFirstSession => data.sessions == 1;

    public static int Sessions
    {
        get => data.sessions;
        set => data.sessions = value;
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void OnRuntimeInitialized()
    {
        Object.DontDestroyOnLoad(new GameObject("Profile", typeof(ProfileMono)));
    }

    public static void SaveLocal()
    {
        PlayerPrefsEx.SetObject("Profile.Data", data);
    }

    public static void LoadLocal()
    {
        data = PlayerPrefsEx.GetObject("Profile.Data", data);
    }

    public static void Reset()
    {
        Application.Quit();
        PlayerPrefs.DeleteAll();
        PlayerPrefsEx.ClearData();
        data = new Data();
        SaveLocal();
    }

    [Console("profile", "display", " : Display all profile data")]
    public static void Display()
    {
        Debug.Log(JsonUtility.ToJson(data, true));
    }

    public static partial class Items
    {
        public static bool Exist(string sku)
        {
            return data.items.Exists(x => x.sku == sku);
        }

        public static bool Add(string sku)
        {
            if (Exist(sku)) return false;
            data.items.Add(new Item() {sku = sku, time = Online.Timer.CurrentSeconds});
            return true;
        }

        public static void Remove(string sku)
        {
            data.items.RemoveAll(x => x.sku == sku);
        }

        public static Item Get(string sku)
        {
            return data.items.Find(x => x.sku == sku);
        }

        public static bool Expired(string sku, long seconds)
        {
            var item = Get(sku);
            if (item == null) return true;
            return (Online.Timer.CurrentSeconds - item.time) >= seconds;
        }
    }
}
