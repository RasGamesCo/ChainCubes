﻿using UnityEngine;
using UnityEditor;


[CustomPropertyDrawer(typeof(RasAd.AdPlace))]
public class AdPlaceDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(rect, label, property);
        rect = EditorGUI.PrefixLabel(rect, label);

        var arrayProp = property.FindPropertyRelative(nameof(RasAd.AdPlace.active));
        rect.width = 40;
        arrayProp.arraySize= EditorGUI.IntField(rect, arrayProp.arraySize);
        rect.x += 45;
        rect.width = 20;
        for (int i = 0; i < arrayProp.arraySize; i++)
        {
            var prop = arrayProp.GetArrayElementAtIndex(i);
            prop.boolValue = EditorGUI.Toggle(rect, prop.boolValue, EditorStyles.toggle);
            rect.x += rect.width;
        }

        var typeProp = property.FindPropertyRelative(nameof(RasAd.AdPlace.type));
        if (typeProp.enumValueIndex == (int) RasAd.AdPlace.Type.Rewarded)
        {
            var rewardProp = property.FindPropertyRelative(nameof(RasAd.AdPlace.reward));

            rect.width = 50;
            EditorGUI.HelpBox(rect, "Reward:", MessageType.None);
            rect.x += 32;
            rect.width = 90;
            rewardProp.intValue = EditorGUI.IntField(rect, rewardProp.intValue);
        }

        EditorGUI.EndProperty();
    }
}
