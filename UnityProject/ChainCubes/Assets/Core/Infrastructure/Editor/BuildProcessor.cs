﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using EyePM;
using EyePM.Builder;


class BuildProcessor : IPreprocessBuildWithReport
{
    public int callbackOrder { get { return 0; } }

    public void OnPreprocessBuild(BuildReport report)
    {
		#if !PLAY_INSTANT
        RasConfig.Instance.version = Builder.Instance.version;
		#endif
        RasConfig.Group = 0;
        EditorUtility.SetDirty(RasConfig.Instance);

        #if GASDK
        #if PLAY_INSTANT
        Debug.Log("GA set Google keys " + RasConfig.ApiKeys.gameAnalyticsGoogle.gameKey + " | " + RasConfig.ApiKeys.gameAnalyticsGoogle.secretKey);
        GameAnalyticsSDK.GameAnalytics.SettingsGA.UpdateGameKey(0, RasConfig.ApiKeys.gameAnalyticsGoogle.gameKey);
        GameAnalyticsSDK.GameAnalytics.SettingsGA.UpdateSecretKey(0, RasConfig.ApiKeys.gameAnalyticsGoogle.secretKey);
        #else
        {
            if (Builder.currentBuilding.config.market == Market.GooglePlay)
            {
                Debug.Log("GA set Google keys " + RasConfig.ApiKeys.gameAnalyticsGoogle.gameKey + " | " + RasConfig.ApiKeys.gameAnalyticsGoogle.secretKey);
                GameAnalyticsSDK.GameAnalytics.SettingsGA.UpdateGameKey(0, RasConfig.ApiKeys.gameAnalyticsGoogle.gameKey);
                GameAnalyticsSDK.GameAnalytics.SettingsGA.UpdateSecretKey(0, RasConfig.ApiKeys.gameAnalyticsGoogle.secretKey);
            }
            else if (Builder.currentBuilding.config.market == Market.Bazaar)
            {
                Debug.Log("GA set Bazaar keys " + RasConfig.ApiKeys.gameAnalyticsBazaar.gameKey + " | " + RasConfig.ApiKeys.gameAnalyticsBazaar.secretKey);
                GameAnalyticsSDK.GameAnalytics.SettingsGA.UpdateGameKey(0, RasConfig.ApiKeys.gameAnalyticsBazaar.gameKey);
                GameAnalyticsSDK.GameAnalytics.SettingsGA.UpdateSecretKey(0, RasConfig.ApiKeys.gameAnalyticsBazaar.secretKey);
            }
            else if (Builder.currentBuilding.config.market == Market.Myket)
            {
                Debug.Log("GA set Myket keys " + RasConfig.ApiKeys.gameAnalyticsMyKet.gameKey + " | " + RasConfig.ApiKeys.gameAnalyticsMyKet.secretKey);
                GameAnalyticsSDK.GameAnalytics.SettingsGA.UpdateGameKey(0, RasConfig.ApiKeys.gameAnalyticsMyKet.gameKey);
                GameAnalyticsSDK.GameAnalytics.SettingsGA.UpdateSecretKey(0, RasConfig.ApiKeys.gameAnalyticsMyKet.secretKey);
            }
            else if(Builder.currentBuilding.config.market == Market.TV)
            {
                Debug.Log("GA set TV keys " + RasConfig.ApiKeys.gameAnalyticsTV.gameKey + " | " + RasConfig.ApiKeys.gameAnalyticsTV.secretKey);
                GameAnalyticsSDK.GameAnalytics.SettingsGA.UpdateGameKey(0, RasConfig.ApiKeys.gameAnalyticsTV.gameKey);
                GameAnalyticsSDK.GameAnalytics.SettingsGA.UpdateSecretKey(0, RasConfig.ApiKeys.gameAnalyticsTV.secretKey);
            }

            EditorUtility.SetDirty(GameAnalyticsSDK.GameAnalytics.SettingsGA);
        }
        #endif
        #endif

        #if !PLAY_INSTANT
        if (IsFinalBuild(Builder.Instance))
        {
            if (ValidateNotifications() == false)
                throw new BuildFailedException(new System.Exception("Please check notifications in RasConfig"));
        }
        #endif
        AssetDatabase.SaveAssets();
    }

    private bool IsFinalBuild(Builder builder)
    {
        return builder.isReleaseCandidate; // && builder.builds.FindAll(x => x.activated).Count > 1;
    }

    private bool ValidateNotifications()
    {
        if (RasConfig.Notifications.items.Count < 1) return false;

        int lastDelay = 0;
        foreach (var item in RasConfig.Notifications.items)
        {
            if (item.delay == lastDelay) return false;
            lastDelay = item.delay;
            if (item.texts.Count < 1 || string.IsNullOrEmpty(item.texts[0])) return false;
        }

        return true;
    }
}
