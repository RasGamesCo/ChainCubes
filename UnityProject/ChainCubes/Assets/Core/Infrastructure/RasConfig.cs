﻿//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////

using System;
using EyePM;
using System.Collections.Generic;
using UnityEngine;


//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////
public partial class RasConfig : StaticConfig<RasConfig>
{
    [SerializeField] private Ras.Config.ServerUrls serverUrls = new Ras.Config.ServerUrls();

    [SerializeField] private Ras.Config.ApiKeys apiKeys = new Ras.Config.ApiKeys();

    [Header("Dynamic Data")]
    #if UNITY_EDITOR
    [InspectorButton("Export Data As", "OnExport")]
    [SerializeField] private bool export = false;
    #endif
    [SerializeField] private Ras.Config.DynamicData dynamicData = new Ras.Config.DynamicData();


    [Header("Static Data"), Space()]
    [SerializeField] private Ras.Config.StaticData staticData = new Ras.Config.StaticData();

    #if UNITY_EDITOR
    [Header("Editor Data"), Space()]
    [SerializeField] private Ras.Config.EditorData editorData = new Ras.Config.EditorData();
    #endif


    protected override void OnInitialize()
    {
        #if PM_CCMD
        EyePM.Console.OnDisplayInfo = str =>
        {
            return "Ver: " + Application.version + " Group: " + Group + "\nId: " + EyePM.Console.DisplayDeviceID;
        };
        #endif
        #if UNITY_EDITOR
        if (editorData.offline)
        {
            Save();
            PlayerPrefsEx.SetInt("GlobalConfig.Group", editorData.offlineGroup);
            export = !export;
        }
        #else
        dynamicData = LoadData(dynamicData);
        #endif
    }

    #if UNITY_EDITOR
    public void OnExport(object sender)
    {
        var path = System.IO.Directory.GetParent(Application.dataPath).Parent.FullName + "/Configs/" + version;
        if (System.IO.Directory.Exists(path) == false)
            System.IO.Directory.CreateDirectory(path);
        var filename = UnityEditor.EditorUtility.SaveFilePanel("Save exported data", path, "config", "txt");
        if (filename.HasContent(4))
            System.IO.File.WriteAllText(filename, JsonUtility.ToJson(dynamicData, false), System.Text.Encoding.UTF8);
    }
    #endif

    ////////////////////////////////////////////////////////////
    /// STATIC MEMBERS
    /// RasGames infrastructure class
    /// !! DO NOT CHANGE THIS FILE !!
    ////////////////////////////////////////////////////////////
    public static Ras.Config.ServerUrls ServerUrls => Instance.serverUrls;
    public static Ras.Config.ApiKeys ApiKeys => Instance.apiKeys;
    public static Ras.Config.DynamicData DynamicData => Instance.dynamicData;
    public static Ras.Config.StaticData StaticData => Instance.staticData;
    public static Ras.Config.Update Update => Instance.dynamicData.update;
    public static Ras.Config.Socials Socials => Instance.dynamicData.socials;
    public static Ras.Config.Gameplay Gameplay => Instance.dynamicData.gameplay[Group % Instance.dynamicData.gameplay.Count];
    public static Ras.Config.Difficulty Difficulty => Instance.dynamicData.difficulty[Group % Instance.dynamicData.difficulty.Count];
    public static Ras.Config.Combo Combo => Instance.dynamicData.combo[Group % Instance.dynamicData.combo.Count];
    public static Ras.Config.Shop Shop => Instance.dynamicData.shop[Group % Instance.dynamicData.shop.Count];
    public static Ras.Config.Rewards Rewards => Instance.dynamicData.rewards[Group % Instance.dynamicData.rewards.Count];
    public static Ras.Config.AdPlaces AdPlaces => Instance.dynamicData.advertises[Group % Instance.dynamicData.advertises.Count];
    public static Ras.Config.AdZones AdZones => Instance.staticData.adZones;
    public static Ras.Config.ProfilePreset ProfilePreset => Instance.dynamicData.profilePreset[Group % Instance.dynamicData.profilePreset.Count];
    public static Ras.Config.Notifications Notifications => Instance.dynamicData.notification[Group % Instance.dynamicData.notification.Count];
    public static Ras.Config.DailyReward DailyReward => Instance.dynamicData.dailyReward[Group % Instance.dynamicData.dailyReward.Count];
    #if METRIX
    public static Ras.Config.MetrixEvents Metrix => Instance.staticData.metrix;
    #endif
    #if UNITY_EDITOR
    public static Ras.Config.EditorData EditorData => Instance.editorData;
    #endif


    public static int Group
    {
        get => PlayerPrefs.GetInt("GlobalConfig.Group", 0);
        set => PlayerPrefs.SetInt("GlobalConfig.Group", value);
    }

    public static bool SetData(string json)
    {
        #if UNITY_EDITOR
        if (Instance.editorData.offline)
            return true;
        #endif
        if (json.IsNullOrEmpty()) return false;

        Ras.Config.DynamicData newdata = null;
        try
        {
            newdata = JsonUtility.FromJson<Ras.Config.DynamicData>(json);
        }
        catch
        {
        }
        if (newdata == null) return false;

        //  set new data
        Instance.dynamicData = newdata;
        Save();

        return true;
    }

    public static void Save()
    {
        SaveData(Instance.dynamicData);
    }

    #if PM_CCMD
    [Console("group", "set", "(index) : Set current group to given index.")]
    public static void SetCurrentGroup(int index)
    {
        Group = index;
    }
    #endif
    #if UNITY_EDITOR
    [UnityEditor.MenuItem("EyePM/Config", priority = 0)]
    private static void SelectMe()
    {
        UnityEditor.Selection.activeObject = Instance;
    }
    #endif
}


namespace Ras.Config
{
    [Serializable]
    public partial class ServerUrls
    {
        [SerializeField] private string config = "http://games.RasGames.ir/OurGameName/";
        public string countries = "http://games.RasGames.ir/globals/countries.txt";
        public string ipInfo = "https://ipinfo.io/json";
        public string ConfigUrl => config + RasConfig.Instance.version + "/config.txt?" + DateTime.Now.Ticks;
    }

    [Serializable]
    public partial class ApiKeys
    {
        [Serializable]
        public partial class GameAnalyticsKeys
        {
            public string gameKey = string.Empty;
            public string secretKey = string.Empty;
        }

        public string tapsell = string.Empty;
        public GameAnalyticsKeys gameAnalyticsTV = new GameAnalyticsKeys();
        public GameAnalyticsKeys gameAnalyticsGoogle = new GameAnalyticsKeys();
        public GameAnalyticsKeys gameAnalyticsBazaar = new GameAnalyticsKeys();
        public GameAnalyticsKeys gameAnalyticsMyKet = new GameAnalyticsKeys();
    }

    [Serializable]
    public partial class RewardData
    {
        [Serializable]
        public partial class Pack
        {
        }

        public List<Pack> packs = new List<Pack>();
    }

    [Serializable]
    public partial class Update
    {
        public enum Mode : int
        {
            Null = 0,
            Soft = 1,
            Force = 2,
        }

        public Mode mode = Mode.Null;
    }

    [Serializable]
    public partial class ProfilePreset
    {

    }

    [Serializable]
    public partial class Socials
    {
        public int rateUsLevel = 2;
        public string privacyPolicyUrl = "https://RasGames.ir/privacy-policy";
        //public string storeUrl = string.Empty;
        //public string rateUrl = string.Empty;
        //public string facebookUrl = string.Empty;
        //public string instagramUrl = string.Empty;
        //public string contactSurveyUrl = string.Empty;
        //public string contactEmailUrl = string.Empty;
    }

    [Serializable]
    public partial class Gameplay
    {
    }

    [Serializable]
    public partial class Combo
    {
    }

    [Serializable]
    public partial class Difficulty
    {
    }

    [Serializable]
    public partial class Shop
    {
    }

    [Serializable]
    public partial class Rewards
    {

    }

    [Serializable]
    public partial class AdPlaces
    {
        public int rewardedMinLevel = 1;
        public int interestitialMinLevel = 4;
    }

    [Serializable]
    public partial class Notifications
    {
        public List<EyePM.LocalNotification.Item> items = new List<EyePM.LocalNotification.Item>();
    }

    [Serializable]
    public partial class DailyReward
    {
        public int timerId = 1;
        public int interval = 24 * 60 * 60;
        public string notif = string.Empty;
        public RewardData reward = new RewardData();
    }

    [Serializable]
    public partial class DynamicData
    {
        public Update update = new Update();
        public Socials socials = new Socials();
        public List<Gameplay> gameplay = new List<Gameplay>() {new Gameplay()};
        public List<Difficulty> difficulty = new List<Difficulty>() {new Difficulty()};
        public List<Combo> combo = new List<Combo>() {new Combo()};
        public List<Shop> shop = new List<Shop>() {new Shop()};
        public List<Rewards> rewards = new List<Rewards>() {new Rewards()};
        public List<AdPlaces> advertises = new List<AdPlaces>() {new AdPlaces()};
        public List<Notifications> notification = new List<Notifications>() {new Notifications()};
        public List<DailyReward> dailyReward = new List<DailyReward>() {new DailyReward()};
        public List<ProfilePreset> profilePreset = new List<ProfilePreset>() {new ProfilePreset()};
    }

    [Serializable]
    public partial class Camera
    {
        [Serializable]
        public partial class ShakeProfiles
        {
            [Serializable]
            public class Profile
            {
                public float duration = 2;
                public Vector2 magnitude = new Vector2(0.2f,0.2f);
            }
        }

        public ShakeProfiles shakeProfiles = new ShakeProfiles();
    }

    [Serializable]
    public partial class Vibration
    {
    }

    [Serializable]
    public partial class AdZones
    {
        public int requestInterval = 1;
        public float rewardedRequestButtonTimeOutDuration = 3;
        [Space]
        public RasAd.Zone banner = new RasAd.Zone();
        public RasAd.Zone rewarded = new RasAd.Zone();
        public RasAd.Zone interstitial = new RasAd.Zone();
    }
    #if METRIX
    [Serializable]
    public partial class MetrixEvents
    {
        public string adShow = string.Empty;
        public string adShowUnique = string.Empty;
        public string adLoad = string.Empty;
        public string adLoadUnique = string.Empty;
        public string adNotAvailable = string.Empty;
        public string adNotAvailableUnique = string.Empty;
        public string adClose = string.Empty;
        public string adCloseUnique = string.Empty;
        public string adBannerShow = string.Empty;
        public string adBannerShowUnique = string.Empty;
        public string adBannerClick = string.Empty;
        public string adBannerClickUnique = string.Empty;
        public string adCrossShow = string.Empty;
        public string adCrossShowUnique = string.Empty;
        public string adCrossClick = string.Empty;
        public string adCrossClickUnique = string.Empty;
        public string levelEnd5 = string.Empty;
        public string levelEnd5Unique = string.Empty;
        public string levelEnd10 = string.Empty;
        public string levelEnd10Unique = string.Empty;
        public string levelEnd20 = string.Empty;
        public string levelEnd20Unique = string.Empty;
        public string levelEnd50 = string.Empty;
        public string levelEnd50Unique = string.Empty;
    }
    #endif

    [Serializable]
    public partial class StaticData
    {
        public float loadingDuration = 2f;
        public Camera camera = new Camera();
        public Vibration vibration = new Vibration();
        public AdZones adZones = new AdZones();
        #if METRIX
        public MetrixEvents metrix = new MetrixEvents();
        #endif
    }

    #if UNITY_EDITOR
    [Serializable]
    public partial class EditorData
    {
        public bool offline = true;
        public int offlineGroup = 0;
        public bool rewardedShowingStartedInEditor = true;
        public bool rewardedShowingSuccessedInEditor = true;
    }
    #endif

}
