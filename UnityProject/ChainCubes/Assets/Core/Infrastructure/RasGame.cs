﻿//////////////////////////////////////////////////////
/// RasGames infrastructure class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////

using System;
using System.Collections;
using UnityEngine;
using EyePM;
using System.Collections.Generic;
using Console = EyePM.Console;


[DefaultExecutionOrder(-500)]
public abstract class RasGame : GameManager
{
    public class PriorityAction
    {
        public int priority = 0;
        public Action onLoaded = null;
    }

    protected virtual void Awake()
    {
        Input.multiTouchEnabled = false;
        #if DEBUG_MODE
        Console.Enabled = true;
        #else
        Console.Enabled = false;
        #endif
    }

    protected virtual IEnumerator Start()
    {
        // loading content from server
        Loading.Show();
        var wait = new WaitForSeconds(RasConfig.StaticData.loadingDuration / 2f);

        Http.DownloadText(RasConfig.ServerUrls.ConfigUrl, null, null, json => RasConfig.SetData(json));

        if (AgeRestriction.NeedData)
        {
            float startTime = Time.time, countryMaxTime = 7, ipinfoMaxTime = 7;
            Http.DownloadText(RasConfig.ServerUrls.countries, result =>
            {
                countryMaxTime = 0;
                AgeRestriction.Countries = result;
            });
            Http.DownloadText(RasConfig.ServerUrls.countries, result =>
            {
                ipinfoMaxTime = 0;
                AgeRestriction.LocationJson = result;
            });
            var delta = Time.time - startTime;
            while (delta < countryMaxTime && delta < ipinfoMaxTime)
                yield return wait;
        }
        else
        {
            yield return wait;
            yield return wait;
        }
        Loading.Hide();
        Initialize();
    }

    private void Initialize()
    {
        actions.Sort((x, y) => x.priority - y.priority);
        foreach (var item in actions)
        {
            try
            {
                item.onLoaded?.Invoke();
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        // link modules together
        RasAd.Initialize(RasConfig.AdZones.banner, RasConfig.AdZones.rewarded, RasConfig.AdZones.interstitial);
        RasAd.OnRequested += zone =>
        {
            if (zone == RasConfig.AdZones.rewarded)
            {
                Debug.Log("______________________________ RasAd.RewardedAd.Requested: " + zone.ZoneId);
                RasAnalytics.Ad.Rewarded.Requested(zone.ZoneId);
            }
            else if (zone == RasConfig.AdZones.interstitial)
            {
                Debug.Log("______________________________ RasAd.Interstitial.Requested: " + zone.ZoneId);
                RasAnalytics.Ad.Interstitial.Requested(zone.ZoneId);
            }
            else
            {
                Debug.Log("______________________________ RasAd.BannerAd.Requested: " + zone.ZoneId);
                RasAnalytics.Ad.Banner.Requested(zone.ZoneId);
            }
        };
        RasAd.OnRequestSucceed += zone =>
        {
            if (zone == RasConfig.AdZones.rewarded)
            {
                Debug.Log("______________________________ RasAd.RewardedAd.RequestSucceed: " + zone.ZoneId);
                RasAnalytics.Ad.Rewarded.RequestSucceed(zone.ZoneId);
            }
            else if (zone == RasConfig.AdZones.interstitial)
            {
                Debug.Log("______________________________ RasAd.InterstitialAd.RequestSucceed: " + zone.ZoneId);
                RasAnalytics.Ad.Interstitial.RequestSucceed(zone.ZoneId);
            }
            else
            {
                Debug.Log("______________________________ RasAd.BannerAd.RequestSucceed: " + zone.ZoneId);
                RasAnalytics.Ad.Banner.RequestSucceed(zone.ZoneId);
            }
            #if METRIX
            RasAnalytics.Metrix.NewEvent(RasConfig.Metrix.adLoad);
            RasAnalytics.Metrix.NewEvent(RasConfig.Metrix.adLoadUnique);
            #endif
        };
        RasAd.OnRequestFailed += (zone, error) =>
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
                return;

            if (zone == RasConfig.AdZones.rewarded)
            {
                Debug.Log("______________________________ RasAd.Rewarded.RequestFailed: " + zone.ZoneId + " With Error Of: " + error);
                RasAnalytics.Ad.Rewarded.Failed(zone.ZoneId);
            }
            else if (zone == RasConfig.AdZones.interstitial)
            {
                Debug.Log("______________________________ RasAd.Interstitial.RequestFailed: " + zone.ZoneId + " With Error Of: " + error);
                RasAnalytics.Ad.Interstitial.Failed(zone.ZoneId);
            }
            else if(zone == RasConfig.AdZones.banner)
            {
                Debug.Log("______________________________ RasAd.Banner.RequestFailed: " + zone.ZoneId + " With Error Of: " + error);
                RasAnalytics.Ad.Banner.Failed(zone.ZoneId);
            }

            #if METRIX
            RasAnalytics.Metrix.NewEvent(RasConfig.Metrix.adNotAvailable);
            RasAnalytics.Metrix.NewEvent(RasConfig.Metrix.adNotAvailableUnique);
            #endif
        };
        RasAd.onPreOpenConfition = adPlace =>
        {
            if (adPlace.type == RasAd.AdPlace.Type.Rewarded)
                return true;
            return true;
        };
        RasAd.OnOpened += adPlace =>
        {
            if (adPlace == null)
            {
                Debug.LogWarning("______________________________ RasAd.NotKnown.Opened --Logically this shouldn't happen, It isn't a good sign");
                return;
            }
            switch (adPlace.type)
            {
                case RasAd.AdPlace.Type.Rewarded:
                    Debug.Log("______________________________ RasAd.Rewarded.Opened: " + adPlace.name);
                    RasAnalytics.Ad.Rewarded.Showed(adPlace.name);
                    break;
                case RasAd.AdPlace.Type.Interstitial:
                    Debug.Log("______________________________ RasAd.Interstitial.Opened: " + adPlace.name);
                    RasAnalytics.Ad.Interstitial.Showed(adPlace.name);
                    break;
                case RasAd.AdPlace.Type.Banner:
                    Debug.Log("______________________________ RasAd.Banner.Opened: " + adPlace.name);
                    RasAnalytics.Ad.Banner.Showed(adPlace.name);
                    break;
                default: throw new ArgumentOutOfRangeException();
            }

            #if METRIX
            if (adPlace.type == RasAd.AdPlace.Type.Banner)
            {
                RasAnalytics.Metrix.NewEvent(RasConfig.Metrix.adBannerShow);
                RasAnalytics.Metrix.NewEvent(RasConfig.Metrix.adBannerShowUnique);
            }
            else
            {
                RasAnalytics.Metrix.NewEvent(RasConfig.Metrix.adShow);
                RasAnalytics.Metrix.NewEvent(RasConfig.Metrix.adShowUnique);
            }
            #endif
        };
        RasAd.OnOpenFailed += adPlace =>
        {
            if (adPlace.type == RasAd.AdPlace.Type.Rewarded)
            {
                Debug.Log("______________________________ RasAd.Rewarded.OpenFailed: " + adPlace.name);
                // Todo: Maybe adding analytics here?
            }
            else if (adPlace.type == RasAd.AdPlace.Type.Interstitial)
            {
                Debug.Log("______________________________ RasAd.Interstitial.OpenFailed: " + adPlace.name);
                // Todo: Maybe adding analytics here?
            }
            else
            {
                Debug.Log("______________________________ RasAd.Banner.Opened: " + adPlace.name);
                // Todo: Maybe adding analytics here?
            }

            #if METRIX
            // Todo: Maybe adding Metrix here?
            #endif
        };
        RasAd.OnClosed += (adPlace, rewarded) =>
        {
            if (rewarded)
            {
                Debug.Log("______________________________ Tapsell.Rewarded.OnCloseAd, Succeed: " + adPlace);
                RasAnalytics.Ad.Rewarded.Succeeded(adPlace.name);
            }
            #if METRIX
            RasAnalytics.Metrix.NewEvent(RasConfig.Metrix.adClose);
            RasAnalytics.Metrix.NewEvent(RasConfig.Metrix.adCloseUnique);
            #endif
        };
        // Schedule other notifications
        EyePM.LocalNotification.Initialize(RasConfig.Notifications.items);
        if (RasConfig.DynamicData.dailyReward.Count > 0 && RasConfig.DailyReward.interval > 10 && RasConfig.DailyReward.notif.HasContent(5))
        {
            EyePM.LocalNotification.OnScheduleNotification += () =>
            {
                // schedule lucky spin push
                var seconds = Online.Timer.GetRemainSeconds(RasConfig.DailyReward.timerId, RasConfig.DailyReward.interval);
                if (seconds > 10)
                {
                    EyePM.LocalNotification.SendNotification(seconds, RasConfig.DailyReward.notif);
                }
            };
        }

        // update remote config
        OnRemoteConfig();
        RasAnalytics.ABTest.OnRecieved = OnRemoteConfig;

        // start session
        if (Profile.IsFirstSession)
            OnFirstSession();
    }

    private void OnRemoteConfig()
    {
        RasConfig.Group = RasAnalytics.ABTest.GetGroup(RasConfig.Group);
        OnRemoteConfigReceived();
        RasConfig.Save();
    }

    protected abstract void OnFirstSession();
    protected abstract void OnRemoteConfigReceived();

    //////////////////////////////////////////////////////
    /// STATIC MEMBERS
    /// RasGames infrastructure class
    /// !! DO NOT CHANGE THIS FILE !!
    //////////////////////////////////////////////////////
    public static List<PriorityAction> actions = new List<PriorityAction>();

    private static RasGame rasGame = default;
    public static RasGame RASGame
    {
        get
        {
            if (rasGame == null)
            {
                rasGame = FindObjectOfType<RasGame>();
                if (rasGame == null)
                {
                    rasGame = new GameObject().AddComponent<RasGame>();
                    rasGame.gameObject.name = "Game";
                    DontDestroyOnLoad(rasGame);
                }
            }
            return rasGame;
        }
    }

    public static void AddOnLoaded(int priority, Action onLoaded)
    {
        actions.Add(new PriorityAction {priority = priority, onLoaded = onLoaded});
    }
}