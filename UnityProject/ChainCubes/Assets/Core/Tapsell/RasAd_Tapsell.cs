﻿#if TAPSELL
using System;
using TapsellSDK;
using UnityEngine;


//////////////////////////////////////////////////////
/// RasGames TapsellAds class
/// !! DO NOT CHANGE THIS FILE !!
//////////////////////////////////////////////////////
public static partial class RasAd
{
    private static AdPlace currentShowingAdPlace = null;

    public partial class Zone
    {
        [HideInInspector] public TapsellAd tapsellAdRequestResult;
    }

    public static partial class Banner
    {
        private static void RequestViaSDK(AdPlace adPlace)
        {
            Tapsell.RequestBannerAd(bannerZone.ZoneId, BannerType.BANNER_320x50, Gravity.BOTTOM, Gravity.CENTER,
                                    zoneId =>
                                    {
                                        bannerZone.CurrentState = Zone.State.Ready;
                                        OnRequestSucceed?.Invoke(bannerZone);
                                        ShowViaSDK(adPlace);
                                    },
                                    error =>
                                    {
                                        bannerZone.CurrentState = Zone.State.Idle;
                                        OnRequestFailed.Invoke(bannerZone, "No Ad Available Error: " + error); // Todo : we may want not to send RequestFailed on not available state, because it sends analytics
                                    },
                                    error =>
                                    {
                                        bannerZone.CurrentState = Zone.State.Idle;
                                        OnRequestFailed.Invoke(bannerZone, "Error: " + error.message);
                                    },
                                    error =>
                                    {
                                        bannerZone.CurrentState = Zone.State.Idle;
                                        OnRequestFailed.Invoke(bannerZone, "No Network Error: " + error); // Todo : we may want not to send RequestFailed on not available state, because it sends analytics
                                    },
                                    error =>
                                    {
                                        bannerZone.CurrentState = Zone.State.Idle;
                                        OnRequestFailed.Invoke(bannerZone, error);
                                        Debug.Log("______________________________  Tapsell.RequestAd.OnHideBanner: Fucking Unknown Reason Error From Tapsell" + error);
                                    });
        }

        private static void ShowViaSDK(AdPlace adPlace)
        {
            OnOpened?.Invoke(adPlace);
            Tapsell.ShowBannerAd(bannerZone.ZoneId);
        }

        private static void HideViaSDK()
        {
            Tapsell.HideBannerAd(bannerZone.ZoneId);
        }
    }

    public static partial class Rewarded
    {
        private static void RequestViaSDK()
        {
            Tapsell.RequestAd(
                              rewardedZone.ZoneId,
                              true, // Todo: should we Cache?
                              tapsellAd =>
                              {
                                  rewardedZone.CurrentState = Zone.State.Ready;
                                  rewardedZone.tapsellAdRequestResult = tapsellAd;
                                  OnRequestSucceed?.Invoke(rewardedZone);
                              },
                              error =>
                              {
                                  rewardedZone.CurrentState = Zone.State.Idle;
                                  OnRequestFailed?.Invoke(rewardedZone, "No Ad Available Error: " + error); // Todo : we may want not to send RequestFailed on not available state, because it sends analytics
                              },
                              error =>
                              {
                                  rewardedZone.CurrentState = Zone.State.Idle;
                                  OnRequestFailed?.Invoke(rewardedZone, "Error: " + error.message);
                              },
                              error =>
                              {
                                  rewardedZone.CurrentState = Zone.State.Idle;
                                  OnRequestFailed?.Invoke(rewardedZone, "No Network Error: " + error); // Todo : we may want not to send RequestFailed on not available state, because it sends analytics
                              },
                              error =>
                              {
                                  rewardedZone.CurrentState = Zone.State.Idle;
                                  OnRequestFailed?.Invoke(rewardedZone, "Expired Error: " + error); // Todo: What is the meaning of expire state?
                              },
                              zid => { OnOpened?.Invoke(currentShowingAdPlace); },
                              zid => { });
        }
    }

    public static partial class Interstitial
    {
        private static void RequestViaSDK()
        {
            Tapsell.RequestAd(
                              interstitialZone.ZoneId,
                              true, // Todo: should we Cache?
                              tapsellAd =>
                              {
                                  interstitialZone.CurrentState = Zone.State.Ready;
                                  interstitialZone.tapsellAdRequestResult = tapsellAd;
                                  OnRequestSucceed?.Invoke(interstitialZone);
                              },
                              error =>
                              {
                                  interstitialZone.CurrentState = Zone.State.Idle;
                                  OnRequestFailed?.Invoke(interstitialZone, "No Ad Available Error: " + error); // Todo : we may want not to send RequestFailed on not available state
                              },
                              error =>
                              {
                                  interstitialZone.CurrentState = Zone.State.Idle;
                                  OnRequestFailed?.Invoke(interstitialZone, "Error: " + error.message); // Todo : we may want not to send RequestFailed on not available state
                              },
                              error =>
                              {
                                  interstitialZone.CurrentState = Zone.State.Idle;
                                  OnRequestFailed?.Invoke(interstitialZone, "No Network Error: " + error); // Todo : we may want not to send RequestFailed on not available state
                              },
                              error =>
                              {
                                  interstitialZone.CurrentState = Zone.State.Idle;
                                  OnRequestFailed?.Invoke(interstitialZone, "Expired Error: " + error); // Todo: What is the meaning of expire state?
                              },
                              zid =>
                              {
                                  interstitialZone.CurrentState = Zone.State.Idle;
                                  OnOpened?.Invoke(currentShowingAdPlace);
                              },
                              zid => { });
        }
    }

    private static void ShowAdViaSDK(Zone zone, AdPlace adPlace, Action<bool, bool> callback) // success and completed, showingStarted
    {
        var options = new TapsellShowOptions(); // Todo: Maybe adding this options to config?
        options.backDisabled = false;
        options.immersiveMode = true;
        options.showDialog = true;
        currentShowingAdPlace = adPlace;
        if (adPlace.type == AdPlace.Type.Rewarded)
        {
            Tapsell.SetRewardListener(result =>
            {
                if (!result.completed)
                {
                    if (zone.IsReady)
                    {
                        zone.CurrentState = Zone.State.Idle;
                        OnClosed?.Invoke(adPlace, false);
                        callback?.Invoke(false, true);
                    }
                }
                else if (result.completed) // although result has rewarded field but it seems to be false always
                {
                    if (zone.IsReady)
                    {
                        zone.CurrentState = Zone.State.Idle;
                        OnClosed?.Invoke(adPlace, true);
                        callback?.Invoke(true, true);
                    }
                }
            });
        }

        Tapsell.ShowAd(zone.tapsellAdRequestResult, zone.ZoneId, options);
    }
}
#endif