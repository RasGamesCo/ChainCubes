﻿using EyePM;
using UnityEngine;
using UnityEngine.UI;


public class Popup_Rateus : GameState
{
    [SerializeField] private GameObject[] stars = null;
    [SerializeField] private Button closeButton = null;
    [SerializeField] private Button sendButton = null;

    private System.Action onCloseFunc = null;

    public Popup_Rateus Setup(System.Action onClose)
    {
        onCloseFunc = onClose;
        return this;
    }

    private void Start()
    {
        Rateus.Current = 0;
        UpdateVisual();
        sendButton.gameObject.SetActive(false);
        UIShowHide.ShowAll(transform);

        closeButton.onClick.AddListener(Back);

        sendButton.onClick.AddListener(() =>
        {
            Back();

            if (Rateus.Current > 2)
            {
                SocialAndSharing.RateUs();
            }
        });
    }

    public void SetRate(int value)
    {
        Rateus.Current = value;
        sendButton.gameObject.SetActive(true);
        closeButton.SetText("LATER");
        UpdateVisual();
    }

    private void UpdateVisual()
    {
        for (int i = 0; i < stars.Length; i++)
            stars[i].SetActive(i < Rateus.Current);
    }

    public override void Back()
    {
        base.Back();
        onCloseFunc?.Invoke();
    }
}


public static class Rateus
{
    public static int Current
    {
        get => PlayerPrefs.GetInt("Rateing.Current", 0);
        set => PlayerPrefs.SetInt("Rateing.Current", value);
    }

    private static int Joy
    {
        get => PlayerPrefs.GetInt("Rateing.Joy", 0);
        set => PlayerPrefs.SetInt("Rateing.Joy", value);
    }

    public static void AddJoy(int value)
    {
        Joy += value;
    }

    public static void CheckJoy(System.Action nextTask = null)
    {
        if (Joy >= 3 && Current < 3)
        {
            Joy *= -1;
            Current = 0;
            Game.Instance.OpenPopup<Popup_Rateus>().Setup(nextTask);
        }
        else nextTask?.Invoke();
    }

    [Console("test", "rateus", "Display RateUs window for test.")]
    public static void Test()
    {
        Current = 0;
        Game.Instance.OpenPopup<Popup_Rateus>();
    }
}
