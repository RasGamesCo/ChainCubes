﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace Ras.Config
{
    public partial class RewardData
    {
        public partial class Pack
        {
            // define packs here
        }
    }

    public partial class ProfilePreset
    {
        public int bomb = 0;
        public int life = 0;
    }

    public partial class Gameplay
    {
        [Serializable]
        public class Bomb
        {
            public float radius = 0.5f;
        }

        [Serializable]
        public class ActiveBaseCube
        {
            public Vector2 bound = new Vector2(-1.65f, 1.65f);
            public float moveSpeedTV = 2;
            public float moveSpeedTouch = 2;
            public float scaleSpeed = 2;
            public float shootForce = 32;
        }

        [Serializable]
        public class MergedCube
        {
            public float forceYMin = 40f;
            public float forceYMax = 60f;
            [Header("No Target Found")]
            public float forceRandomMin = -30f;
            public float forceRandomMax = 30f;
            [Header("Target Found")]
            public float forceToTargetCube = 20f;
            [Space]
            public float angularForce = 5f;
            public float waitForMerged = 2f;
        }

        public ActiveBaseCube activeBaseCube = new ActiveBaseCube();
        public Bomb bomb = new Bomb();
        public MergedCube mergedCube = new MergedCube();
        [Space]
        public float checkGameOverTime = 0.15f;
        public float loadFromScratchTimeScale = 2f;
    }

    public partial class Combo
    {
        // add fields here
    }

    public partial class Difficulty
    {
        // add fields here
    }

    public partial class Shop
    {
        // add fields here
    }

    public partial class Rewards
    {
        public float delayToShowPopup = 2f;
        [Range(0f, 1f)]
        public float bombToLifePercent = 0.5f;
    }

    public partial class AdPlaces
    {
        [Space]
        public RasAd.AdPlace mainBanner = new RasAd.AdPlace(RasAd.AdPlace.Type.Banner, "mainBanner");
        public RasAd.AdPlace loseInterstitial = new RasAd.AdPlace(RasAd.AdPlace.Type.Interstitial,"Lose-Interstitial", 3);
        public RasAd.AdPlace newBigCubeInterstitial = new RasAd.AdPlace(RasAd.AdPlace.Type.Interstitial, "Pre-Loose", 3);
        public RasAd.AdPlace newBigCubeRewardClaimRewarded = new RasAd.AdPlace(RasAd.AdPlace.Type.Rewarded, "Reward-Claim");
        public RasAd.AdPlace powerUpRequestRewarded = new RasAd.AdPlace(RasAd.AdPlace.Type.Rewarded, "PowerUp-Request");
        public RasAd.AdPlace reviveRewarded = new RasAd.AdPlace(RasAd.AdPlace.Type.Rewarded, "PowerUp-Request");
    }

    public partial class Camera
    {
        public partial class ShakeProfiles
        {
            // add fields here
            public Profile bombExplosion = new Profile();
            public Profile lose = new Profile();
        }
    }

    public partial class Vibration
    {
        public int cubeMerge = 120;
        public int bombExplosion = 300;
        public int lifeExplosion = 200;
        public int lose = 200;
    }

    public partial class StaticData
    {
        [Serializable]
        public class BaseCube
        {
            [Serializable]
            public struct FontSize
            {
                [Serializable]
                public struct DigitBasedFontSize
                {
                    public int digitCount;
                    public int size;
                }

                [SerializeField] private int defaultSize;
                [SerializeField] private List<DigitBasedFontSize> digitBasedFontSizes;

                public int GetFontSizeOf(string value)
                {
                    int digitCount = value.Length;
                    var digitBasedSize = digitBasedFontSizes.Find(size => size.digitCount == digitCount);
                    return digitBasedSize.size == 0 ? defaultSize : digitBasedSize.size;
                }
            }

            public float minStabilityDuration = 0.3f;
            public float maxStabilityVelocity = 0.1f;

            [Space]
            public FontSize fontSize = new FontSize();
        }

        public BaseCube baseCubes = new BaseCube();
    }


    #if UNITY_EDITOR
    public partial class EditorData
    {
        public int bomb = -1;
        public int life = -1;
        public int preMadeBoard = -1;
    }
    #endif
}