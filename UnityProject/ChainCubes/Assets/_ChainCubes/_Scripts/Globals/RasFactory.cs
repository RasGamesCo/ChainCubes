﻿using System;
using EyePM;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public partial class RasFactory
{
    public partial class SoundEffects
    {
        public AudioClip win2 = null;
        public AudioClip shoot = null;
        public AudioClip baseCubeMerge = null;
        public AudioClip bigBaseCubeMerge = null;
        public AudioClip newBigCubePopup = null;
        public AudioClip useBomb = null;
        public AudioClip useLife = null;
        public AudioClip bombExplosion = null;
        public AudioClip lifeExplosion = null;
        public AudioClip shakePlayButton = null;
        public AudioClip rewardClaimSuccess = null;
        public AudioClip powerUpRequestSuccess = null;
        public AudioClip levelStartBounce = null;
    }


    [Serializable]
    private class BaseCubeProperties
    {
        [SerializeField] private List<Material> materials = new List<Material>();
        [SerializeField] private Material randomColorMaterial = null;
        [SerializeField] private PhysicMaterial bouncyPhysicMaterial = null;
        [SerializeField] private PhysicMaterial normalPhysicMaterial = null;

        public List<Material> Materials => materials;
        public Material RandomColorMaterial => randomColorMaterial;
        public PhysicMaterial BouncyPhysicMaterial => bouncyPhysicMaterial;
        public PhysicMaterial NormalPhysicMaterial => normalPhysicMaterial;
    }

    [SerializeField] private BaseCubeProperties baseCubeProperties = new BaseCubeProperties();

    private static readonly int Color = Shader.PropertyToID("_Color");

    public static class Boards
    {
        private static List<ResourceEx.File> all = new List<ResourceEx.File>();

        public static List<ResourceEx.File> All
        {
            get
            {
                if (all.Count < 1)
                    all = ResourceEx.LoadAll("Prefabs/Boards/", false);
                return all;
            }
        }

        public static Transform CreateWinParticle(Transform parent)
        {
            return ResourceEx.Load<Transform>("Prefabs/WinParticle").Clone<Transform>(parent);
        }

        public static Transform CreateEmpty(Transform parent)
        {
            return ResourceEx.Load<Transform>("Prefabs/EmptyBoard").Clone<Transform>(parent);
        }

        public static Transform CreateRandomPreMade(Transform parent)
        {
            string selectedBoardPath = All.RandomOne().path;
            #if UNITY_EDITOR
            if (RasConfig.EditorData.preMadeBoard != -1)
                selectedBoardPath = All[RasConfig.EditorData.preMadeBoard].path;
            #endif
            return ResourceEx.Load<Transform>(selectedBoardPath).Clone<Transform>(parent);
        }
    }

    public static class PowerUp
    {
        public static Transform Create(Transform parent, State_Playing.PowerUpType type)
        {
            return ResourceEx.Load<Transform>("Prefabs/" + type).Clone<Transform>(parent);
        }
    }

    public static class ReviveBox
    {
        public static Transform Create(Transform parent)
        {
            return ResourceEx.Load<Transform>("Prefabs/ReviveBox").Clone<Transform>(parent);
        }
    }

    public static class BaseCubes
    {
        public static PhysicMaterial BouncyPhysicMaterial => Instance.baseCubeProperties.BouncyPhysicMaterial;
        public static PhysicMaterial NormalPhysicMaterial => Instance.baseCubeProperties.NormalPhysicMaterial;
        private static List<Material> Materials => Instance.baseCubeProperties.Materials;
        private static Material RandomColorMaterial => Instance.baseCubeProperties.RandomColorMaterial;

        public static Transform CreateTrail(Transform parent)
        {
            return ResourceEx.Load<Transform>("Prefabs/BaseCubeTrail").Clone<Transform>(parent);
        }

        public static void CreateMergeParticle(Transform parent, Vector3 position)
        {
            ResourceEx.Load<Transform>("Prefabs/CubesMergeParticle").Clone<Transform>(parent).position = position;
        }

        public static BaseCube Create(Transform parent)
        {
            return ResourceEx.Load<BaseCube>("Prefabs/BaseCube").Clone<BaseCube>(parent);
        }

        public static Material GetMaterial(long value)
        {
            return GetMaterial(value, out Color _);
        }

        public static Material GetMaterial(long value, out Color color)
        {
            int index = (int) Mathf.Log(value, 2) - 1;
            if (index < Materials.Count && index >= 0)
            {
                color = Materials[index].GetColor(Color);
                return Materials[index];
            }

            Material newMat = new Material(RandomColorMaterial);
            Random.InitState((int) value * 1);
            float rand1 = Random.value;
            Random.InitState((int) value * 2);
            float rand2 = Random.value;
            Random.InitState((int) value * 3);
            float rand3 = Random.value;
            Color randomColor = new Color(rand1, rand2, rand3);

            newMat.SetColor(Color, randomColor);
            color = randomColor;
            return newMat;
        }
    }
}