﻿using UnityEngine;
using UnityEngine.UI;


public class TVToggle : TVInteractable, ITVInteractable
{
    [SerializeField] [HideInInspector] private Toggle unityToggle = null;

    private Toggle UnityToggle => unityToggle ? unityToggle : GetComponent<Toggle>();

    public override void Click()
    {
        base.Click();
        UnityToggle.isOn = !UnityToggle.isOn;
    }
}
