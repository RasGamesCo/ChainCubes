﻿using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
public class TVButton : TVInteractable, ITVInteractable
{
    [SerializeField] [HideInInspector] private Button unityBtn = null;

    private Button UnityBtn => unityBtn ? unityBtn : GetComponent<Button>();

    public override void Click()
    {
        base.Click();
        UnityBtn.onClick.Invoke();
    }
}
