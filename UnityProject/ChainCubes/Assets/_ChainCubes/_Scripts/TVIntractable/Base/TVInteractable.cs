﻿using EyePM;
using UnityEngine;
using UnityEngine.UI;


[AddComponentMenu("")] // This component should not be added to any gameObject directly, so we are hiding it :D
public class TVInteractable : MonoBehaviour
{
    [Tooltip("0,0 Means Bottom Left")]
    [SerializeField] private Vector2 gridPos = Vector2.zero;
    [SerializeField] private Image hoverEffect = null;

    private Vector2Int gridPosInt = new Vector2Int(-1, -1);

    public Vector2Int GridPos()
    {
        if (gridPosInt == new Vector2Int(-1, -1))
            gridPosInt = new Vector2Int((int) gridPos.x, (int) gridPos.y);
        return gridPosInt;
    }

    private void Awake()
    {
        #if !TV
        Destroy(this);
        #endif
        hoverEffect.enabled = false;
    }

    public virtual void ToggleHover(bool state, bool withSound = true)
    {
        hoverEffect.enabled = state;
        if (state && withSound)
            AudioManager.PlaySound(RasFactory.Sounds.buttonHover);
    }

    public virtual void Click()
    {

    }

    public virtual bool IsActive()
    {
        return gameObject.activeInHierarchy;
    }
}