﻿using UnityEngine;


public interface ITVInteractable
{
    Vector2Int GridPos();
    void Click();
    void ToggleHover(bool state, bool withSound = true);
    bool IsActive();
}