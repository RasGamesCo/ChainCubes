﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;


// TODO: Remained Feathers : 1. Support InputField 2.Support Scroll 3.Dynamic (exposed radius for finding nearest)

public class TVInteractableHandler : MonoBehaviour
{
    public Action onGettingEnabled = delegate {  };

    [SerializeField] private List<TVInteractable> allInteractables = new List<TVInteractable>();
    [SerializeField] [HideInInspector] private int columnCount = 0;
    [SerializeField] [HideInInspector] private int rowCount = 0;

    private ITVInteractable[,] allInteractablesSorted;
    private ITVInteractable activeInteractable = null;

    private void Awake()
    {
        #if !TV
        Destroy(this);
        return;
        #endif
        PushHandler(this);
    }

    private void OnDestroy()
    {
        PopHandler(this);
    }

    private void Start()
    {
        if (allInteractables.Count == 0 || (rowCount == 0 && columnCount == 0))
        {
            Debug.LogError("No TV Interactable Found, Maybe Its Not Setuped");
            enabled = false;
            return;
        }
        FillSortedArray();

        foreach (ITVInteractable interactable in allInteractablesSorted)
            interactable?.ToggleHover(false);
        activeInteractable = (ITVInteractable) allInteractables.Where(interactable => interactable.IsActive()).ToList()[0];
        HoverOn(null, activeInteractable, false);
    }

    private void OnEnable()
    {
        HoverOn(null, activeInteractable, false);
        onGettingEnabled.Invoke();
    }

    private void OnDisable()
    {
        HoverOn(activeInteractable, null, false);
    }

    private void FillSortedArray()
    {
        allInteractablesSorted = new ITVInteractable[columnCount, rowCount];
        for (int j = 0; j < rowCount; j++)
        {
            for (int i = 0; i < columnCount; i++)
            {
                var existBtn = allInteractables.Find(interactable => interactable.GridPos().x == i && interactable.GridPos().y == j);
                allInteractablesSorted[i, j] = existBtn ? existBtn.GetComponent<ITVInteractable>() : null;
            }
        }
    }

    private void Update()
    {
        // TODO : Now that i re thought, i would say using key code is much better, 'cause it will be a Much more ready to use Pakcage
        if (Input.GetButtonDown("UpArrow"))
            Move(Vector2Int.up);
        else if (Input.GetButtonDown("DownArrow"))
            Move(Vector2Int.down);
        else if (Input.GetButtonDown("RightArrow"))
            Move(Vector2Int.right);
        else if (Input.GetButtonDown("LeftArrow"))
            Move(Vector2Int.left);
        else if (Input.GetButtonDown("Submit"))
            Click();
    }

    private void Click()
    {
        activeInteractable.Click();
    }

    private void Move(Vector2Int direction)
    {
        if (direction == Vector2Int.zero)
            return;

        for (Vector2Int ij = activeInteractable.GridPos() + direction; 0 <= ij.x && 0 <= ij.y && ij.y < rowCount && ij.x < columnCount; ij += direction)
            if (TryToHoverOn(activeInteractable.GridPos(), ij))
                return;

        ITVInteractable nearestFound = GetNearestOfInDirection(activeInteractable, direction);
        if (nearestFound != null)
            HoverOn(activeInteractable, nearestFound);
    }

    private bool TryToHoverOn(Vector2Int from, Vector2Int to)
    {
        if (allInteractablesSorted[to.x, to.y] == null || !allInteractablesSorted[to.x, to.y].IsActive())
            return false;
        HoverOn(allInteractablesSorted[from.x, from.y], allInteractablesSorted[to.x, to.y]);
        return true;
    }

    private void HoverOn([CanBeNull] ITVInteractable from, [CanBeNull] ITVInteractable to, bool withSound = true)
    {
        from?.ToggleHover(false);
        to?.ToggleHover(true, withSound);
        if (to != null)
            activeInteractable = to;
    }

    private ITVInteractable GetNearestOfInDirection(ITVInteractable source, Vector2Int direction)
    {
        if (direction == Vector2Int.zero)
            return null;

        ITVInteractable nearestFound = null;
        float lowestFoundDistance = Single.PositiveInfinity;
        foreach (var tvInteractable in allInteractables)
        {
            var interactable = (ITVInteractable) tvInteractable;
            if (!interactable.IsActive())
                continue;
            if ((direction.y == 0 || ((direction.y > 0 ? 1 : -1) * (interactable.GridPos().y - source.GridPos().y)) > 0)
             && (direction.x == 0 || ((direction.x > 0 ? 1 : -1) * (interactable.GridPos().x - source.GridPos().x)) > 0))
            {
                float distance = GetDistanceOf(interactable, source);
                if (distance < lowestFoundDistance)
                {
                    nearestFound = interactable;
                    lowestFoundDistance = distance;
                }
            }
        }
        return nearestFound;
    }

    private float GetDistanceOf(ITVInteractable from, ITVInteractable to)
    {
        return (to.GridPos() - from.GridPos()).magnitude;
    }

    #if UNITY_EDITOR
    public void Setup()
    {
        rowCount = 0;
        columnCount = 0;
        allInteractables = transform.GetComponentsInChildren<TVInteractable>().ToList();
        CalculateRowAndColumnsCount();
    }

    private void CalculateRowAndColumnsCount()
    {
        foreach (TVInteractable t in allInteractables)
        {
            if (t.GridPos().y >= rowCount)
                rowCount = t.GridPos().y + 1;
            if (t.GridPos().x >= columnCount)
                columnCount = t.GridPos().x + 1;
        }
    }
    #endif

    //////////////////////////////////////////////////////
    /// STATIC MEMBERS
    //////////////////////////////////////////////////////
    private static readonly List<TVInteractableHandler> Handlers = new List<TVInteractableHandler>();

    private static TVInteractableHandler activeHandler = null;
    private static TVInteractableHandler ActiveHandler
    {
        set
        {
            if (value == activeHandler)
                return;
            if (activeHandler)
                activeHandler.enabled = false;
            activeHandler = value;
            if (activeHandler)
                activeHandler.enabled = true;
        }
    }

    private static void PushHandler(TVInteractableHandler handler)
    {
        Handlers.Add(handler);
        ActiveHandler = handler;
    }

    private static void PopHandler(TVInteractableHandler handler)
    {
        if (Handlers.Count == 0)
        {
            Debug.LogError("No Handlers, But you are trying to remove one! check why its now added to list before removing, or maybe it's requesting to remove more than once");
            return;
        }
        if (Handlers.Contains(handler) == false)
        {
            Debug.LogError("Requesting to Remove a TVInteractableHandler which does not exist in the list, Maybe its Already Removed and requesting more than once, ore maybe it's not added yet!");
            return;
        }
        if (Handlers.Count == 1)
            activeHandler = null;
        else if (activeHandler == handler)
            ActiveHandler = Handlers[Handlers.Count - 1] == handler ? Handlers[Handlers.Count - 2] : Handlers[Handlers.Count - 1];

        Handlers.Remove(handler);
    }
}