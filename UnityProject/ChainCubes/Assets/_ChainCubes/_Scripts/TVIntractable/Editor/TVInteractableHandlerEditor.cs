﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TVInteractableHandler))]
public class TVInteractableHandlerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space(5);
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        if (GUILayout.Button("Setup"))
        {
            (target as TVInteractableHandler)?.Setup();
            serializedObject.Update();
            EditorUtility.SetDirty(target);
        }
        EditorGUILayout.Space(17);
    }
}
