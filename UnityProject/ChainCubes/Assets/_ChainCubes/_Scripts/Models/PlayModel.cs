﻿using System.Collections.Generic;


public static partial class PlayModel
{
    public partial class Stats
    {
        public long score = 0;
        public long currentBestCubeValue = 0;
        public ActiveBaseCube activeBaseCube = null;
        public List<BaseCube> baseCubes = new List<BaseCube>();
    }

    public static long highScore = 0;

    public static void Reset()
    {
        highScore = 0;
        stats = new Stats();
        level = new LevelModel();
        reward = new RewardModel();
        onLeaveRequest = null;
        onLoosing = null;
        onLosed = null;
        onRestartRequest = null;
    }

    public static System.Action<System.Action> onRestartRequest = null;
    public static System.Action onLeaveByClosingApplication = null;
}
