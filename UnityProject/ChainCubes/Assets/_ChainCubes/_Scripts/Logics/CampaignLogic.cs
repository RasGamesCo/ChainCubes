﻿using System.Linq;
using EyePM;
using UnityEngine;


public static class CampaignLogic
{
    public static void ResetGame()
    {
        PlayModel.Reset();

        PlayModel.stats.score = Profile.Score;
        PlayModel.stats.currentBestCubeValue = Profile.CurrentBestCubeValue;
        PlayModel.highScore = Profile.HighScore;
    }

    public static void StartGame()
    {
        PlayModel.onLeaveRequest = OnLeaveRequest;
        PlayModel.onLeaveByClosingApplication = OnLeaveByClosingApplication;
        PlayModel.onLoosing = OnLosing;
        PlayModel.onLosed = OnLoosed;
        PlayModel.onRestartRequest = OnRestartRequest;

        // start game here
        Game.Instance.OpenState<State_Playing>();
    }

    private static void OnLeaveRequest(System.Action exitPlaying) // from back in state Playing , or from setting In Game Popup Main Menu Button
    {
        OnLeave();
        exitPlaying?.Invoke();
        Game.Instance.OpenState<State_MainMenu>();
    }

    private static void OnLeaveByClosingApplication()
    {
        OnLeave();
        Profile.SaveLocal();
    }

    private static void OnLeave()
    {
        Profile.GameQuited(PlayModel.stats.score,PlayModel.stats.currentBestCubeValue, PlayModel.stats.activeBaseCube.GetComponent<BaseCube>(), PlayModel.stats.baseCubes);
    }

    private static void OnLosing(System.Action<bool> exitPlaying)
    {
        bool newHighScore = Profile.GameLoosing(PlayModel.stats.score);
        if (newHighScore)
            RasFactory.Boards.CreateWinParticle(Playground.Instance.transform);
        AudioManager.PlaySound(newHighScore ? (Random.value > 0.5f ? RasFactory.Sounds.win : RasFactory.Sounds.win2) : RasFactory.Sounds.lose);
        Game.Instance.OpenPopup<Popup_Prelose>().Setup(PlayModel.stats.score, PlayModel.stats.baseCubes.Max(cube => cube.Value), newHighScore,
                                                       keepMove => { exitPlaying?.Invoke(keepMove == false); });
    }

    private static void OnLoosed(System.Action exitPlaying)
    {
        exitPlaying?.Invoke();
    }

    private static void OnRestartRequest(System.Action exitPlaying)
    {
        Profile.GameLoosedOrRestarted(PlayModel.stats.score);
        exitPlaying?.Invoke();
        Game.Instance.OpenState<State_Main>();
    }

}