﻿using EyePM;
using UnityEngine;


[DefaultExecutionOrder(-99999)]
public class Settings : MonoBehaviour
{
    [TextArea(3, 5)]
    [SerializeField] private string terms = string.Empty;

    [System.Serializable]
    public class Data
    {
        public string terms = string.Empty;
        public bool termsAgree = false;
        public bool vibration = true;
    }

    private void Awake()
    {
        LoadData();
        data.terms = terms;
    }

    private void OnApplicationQuit()
    {
        SaveData();
    }

    private void OnApplicationPause(bool pause)
    {
        SaveData();
    }

    //////////////////////////////////////////////////////
    /// STATIC MEMBERS
    //////////////////////////////////////////////////////
    private static Data data = new Data();

    public static string TermsDescription => data.terms;

    public static bool TermsAgree
    {
        get => data.termsAgree;
        set => data.termsAgree = value;
    }

    public static bool Vibration
    {
        get => data.vibration;
        set => data.vibration = value;
    }

    private static void LoadData()
    {
        data = PlayerPrefsEx.GetObject("Settings.Data", data);
    }

    private static void SaveData()
    {
        PlayerPrefsEx.SetObject("Settings.Data", data);
    }

}
