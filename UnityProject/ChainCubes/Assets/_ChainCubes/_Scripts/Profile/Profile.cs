﻿using System;
using System.Collections.Generic;
using Ras.Profile;
using JetBrains.Annotations;
using UnityEngine;


public static partial class Profile
{
    public static void StartFirstSession()
    {
        GameLoosedOrRestarted(0);

        data.bomb = 0;
        data.life = 0;

        HighScore = 0;
        Score = 0;
        CurrentBestCubeValue = 0;
        IsPlaying = false;

        EarnPowerUp(State_Playing.PowerUpType.Bomb, RasConfig.ProfilePreset.bomb);
        EarnPowerUp(State_Playing.PowerUpType.Life, RasConfig.ProfilePreset.life);
    }

    #if UNITY_EDITOR
    public static void SetPowerUp(State_Playing.PowerUpType type, int value)
    {
        switch (type)
        {
            case State_Playing.PowerUpType.Bomb:
                data.bomb = value;
                break;
            case State_Playing.PowerUpType.Life:
                data.life = value;
                break;
            default: throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }
    #endif

    public static void EarnPowerUp(State_Playing.PowerUpType type, int amount)
    {
        switch (type)
        {
            case State_Playing.PowerUpType.Bomb:
                data.bomb += amount;
                break;
            case State_Playing.PowerUpType.Life:
                data.life += amount;
                break;
            default: throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }

    public static void SpendPowerUp(State_Playing.PowerUpType type, int amount)
    {
        switch (type)
        {
            case State_Playing.PowerUpType.Bomb:
                data.bomb = Mathf.Max(0, data.bomb - amount);
                break;
            case State_Playing.PowerUpType.Life:
                data.life = Mathf.Max(0, data.life - amount);
                break;
            default: throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }

    public static int GetPowerUp(State_Playing.PowerUpType type)
    {
        switch (type)
        {
            case State_Playing.PowerUpType.Bomb:
                return data.bomb;
            case State_Playing.PowerUpType.Life:
                return data.life;
            default: throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }

    public static bool IsPlaying
    {
        get => data.isPlaying;
        private set => data.isPlaying = value;
    }

    public static long HighScore
    {
        get => data.highScore;
        private set => data.highScore = value;
    }

    public static long Score
    {
        get => data.score;
        private set => data.score = value;
    }

    public static long CurrentBestCubeValue
    {
        get => data.currentBestCubeValue;
        private set => data.currentBestCubeValue = value;
    }

    public static List<long> ShownRewards => data.shownRewards;

    public static void AddShownRewards(long value)
    {
        if (ShownRewards.Contains(value))
            return;
        data.shownRewards.Add(value);
    }

    private static void ClearShownRewards()
    {
        data.shownRewards.Clear();
    }

    public static long ActiveBaseCubeValue => data.activeBaseCubeValue;
    public static List<Data.BaseCubeProfileData> BaseCubes => data.baseCubes;

    public static void GameQuited(long score, long currentBestCubeValue, [CanBeNull] BaseCube activeBaseCubeValue, List<BaseCube> baseCubes) // Saving Level , goto mainMenu
    {
        IsPlaying = true;
        Score = score;
        CurrentBestCubeValue = currentBestCubeValue;
        data.activeBaseCubeValue = activeBaseCubeValue == null ? 0 : activeBaseCubeValue.Value;
        data.baseCubes = ConvertBaseCubesToData(baseCubes);
    }

    public static bool GameLoosing(long score)
    {
       return GameLoosedOrRestarted(score);
    }

    public static bool GameLoosedOrRestarted(long score)
    {
        bool newHighScore = false;
        IsPlaying = false;
        if (score > HighScore)
        {
            HighScore = score;
            newHighScore = true;
        }
        Score = 0;
        CurrentBestCubeValue = 0;
        ClearShownRewards();
        data.activeBaseCubeValue = 0;
        data.baseCubes = new List<Data.BaseCubeProfileData>();
        return newHighScore;
    }

    private static List<Data.BaseCubeProfileData> ConvertBaseCubesToData(List<BaseCube> baseCubes)
    {
        List<Data.BaseCubeProfileData> result = new List<Data.BaseCubeProfileData>(baseCubes.Count);
        foreach (BaseCube baseCube in baseCubes)
        {
            Data.BaseCubeProfileData newData = new Data.BaseCubeProfileData();
            newData.value = baseCube.Value;
            newData.velocity = baseCube.Velocity;
            newData.position = baseCube.transform.position;
            newData.rotation = baseCube.transform.eulerAngles;

            result.Add(newData);
        }

        return result;
    }
}
