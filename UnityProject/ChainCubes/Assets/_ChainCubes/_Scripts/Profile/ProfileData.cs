﻿using System;
using System.Collections.Generic;
using EyePM;
using UnityEngine;


namespace Ras.Profile
{
    public partial class Data
    {
        [Serializable]
        public class BaseCubeProfileData
        {
            public long value = 0;
            public Vector3 velocity = Vector3.zero;
            public Vector3 position = Vector3.zero;
            public Vector3 rotation = Vector3.zero;
        }

        public CryptoInt bomb = 0;
        public CryptoInt life = 0;
        public bool isPlaying = false;
        public long highScore = 0;
        public long score = 0;
        public long currentBestCubeValue = 0;
        public List<long> shownRewards = new List<long>();
        public long activeBaseCubeValue = 0;
        public List<BaseCubeProfileData> baseCubes = new List<BaseCubeProfileData>();
    }
}
