﻿using System.Collections;
using UnityEngine;


namespace EyePM
{
    public class Game : RasGame
    {
        protected override IEnumerator Start()
        {
            yield return base.Start();

            #if UNITY_EDITOR
            if (RasConfig.EditorData.bomb != -1)
                Profile.SetPowerUp(State_Playing.PowerUpType.Bomb, RasConfig.EditorData.bomb);

            if (RasConfig.EditorData.life != -1)
                Profile.SetPowerUp(State_Playing.PowerUpType.Life, RasConfig.EditorData.life);
            #endif

            if (Profile.IsPlaying)
                OpenState<State_Main>();
            else
                OpenState<State_MainMenu>();

            AudioManager.PlayMusic(Random.Range(0, 100), 2, 2);
        }

        protected override void OnFirstSession()
        {
            Profile.StartFirstSession();
        }

        protected override void OnRemoteConfigReceived()
        {
            // place code here
            //RasConfig.Gameplay.movement.moveSpeed = RasAnalytics.ABTest.Get("speed", RasConfig.Gameplay.movement.moveSpeed);
        }

        //////////////////////////////////////////////////////
        /// STATIC MEMBERS
        //////////////////////////////////////////////////////
        private static Game instance = null;
        public static Game Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<Game>();
                    if (instance == null)
                    {
                        instance = new GameObject().AddComponent<Game>();
                        instance.gameObject.name = instance.GetType().Name;
                    }
                }
                return instance;
            }
        }
    }
}
