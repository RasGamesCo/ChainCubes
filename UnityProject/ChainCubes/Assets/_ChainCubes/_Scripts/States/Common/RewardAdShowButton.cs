using System;
using System.Collections;
using System.Collections.Generic;
using EyePM;
using UnityEngine;
using UnityEngine.UI;


namespace Ras.RewardedAdShowButton
{
    public enum State
    {
        Normal,
        Loading,
        FailedToShow
    }

    [ExecuteAlways]
    [RequireComponent(typeof(Button))]
    public class RewardAdShowButton : MonoBehaviour
    {
        [Serializable]
        private class IdleModeItem
        {
            public GameObject gameObject;
            public bool DefaultActiveness { set; get; }

            public IdleModeItem(GameObject gameObject)
            {
                this.gameObject = gameObject;
            }
        }

        [SerializeField] private List<IdleModeItem> idleModeItems = new List<IdleModeItem>();
        [SerializeField] private Button mainButton = null;
        [SerializeField] private RewardedAdShowButtonContent content = null;

        public Button MainButton => mainButton;

        private void Awake()
        {
            #if UNITY_EDITOR
            if (Application.isPlaying)
                #endif
                Init();
        }

        private void Init()
        {
            foreach (IdleModeItem item in idleModeItems)
                item.DefaultActiveness = item.gameObject.activeInHierarchy;
            SetState(State.Normal);
        }

        public void SetState(State state)
        {
            mainButton.SetInteractable(state != State.Loading);
            content.SetState(state);
            foreach (IdleModeItem item in idleModeItems)
                item.gameObject.SetActive(state == State.Normal && item.DefaultActiveness);

            StopCoroutine(FailedToShow());
            if (state == State.Loading)
                StartCoroutine(FailedToShow());
        }

        private IEnumerator FailedToShow()
        {
            yield return new WaitForSecondsRealtime(RasConfig.AdZones.rewardedRequestButtonTimeOutDuration);
            SetState(State.FailedToShow);
        }

        #if UNITY_EDITOR
        private void Update()
        {
            if (!mainButton && !content)
                Reset();
        }

        private void Reset()
        {
            foreach (RewardedAdShowButtonContent item in GetComponentsInChildren<RewardedAdShowButtonContent>())
                DestroyImmediate(item.gameObject);
            mainButton = GetComponent<Button>();

            idleModeItems.Clear();
            for (int i = 0; i < mainButton.transform.childCount; i++)
                idleModeItems.Add(new IdleModeItem(mainButton.transform.GetChild(i).gameObject));

            content = Instantiate(Resources.Load<RewardedAdShowButtonContent>(nameof(RewardedAdShowButtonContent)), transform);
        }

        #endif
    }
}