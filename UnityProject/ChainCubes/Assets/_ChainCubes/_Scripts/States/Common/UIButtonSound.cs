﻿using EyePM;
using UnityEngine;
using UnityEngine.UI;

// TODO: @jamasb update and Put this script to frame work
[DisallowMultipleComponent]
public class UIButtonSound : MonoBehaviour
{
    private void Start()
    {
        var button = GetComponent<Button>();
        var toggle = GetComponent<Toggle>();
        if (button)
            button.onClick.AddListener(() => AudioManager.PlaySound(RasFactory.Sounds.buttonClick));
        else if(toggle)
            toggle.onValueChanged.AddListener(arg0 => AudioManager.PlaySound(RasFactory.Sounds.buttonClick));
        else
            Destroy(this);
    }
}
