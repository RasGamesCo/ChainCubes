using System.Collections;
using UnityEngine;
using UnityEngine.UI;


namespace Ras.RewardedAdShowButton
{
    [AddComponentMenu("")]
    public class RewardedAdShowButtonContent : MonoBehaviour
    {
        [SerializeField] private GameObject retryPack = null;
        [SerializeField] private GameObject loadingPack = null;
        [SerializeField] private Text loadingText = null;

        private IEnumerator Start()
        {
            int index = 0;
            string p;
            while (true)
            {
                p = "";
                for (int i = 0; i < index; i++)
                    p += '.';
                loadingText.text = "Loading" + p;
                index++;
                if (index > 3)
                    index = 0;
                yield return new WaitForSecondsRealtime(0.5f);
            }
        }

        public void SetState(State state)
        {
            loadingPack.SetActive(state == State.Loading);
            retryPack.SetActive(state == State.FailedToShow);
        }
    }
}