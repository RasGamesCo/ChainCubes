﻿using EyePM;
using UnityEngine;
using UnityEngine.UI;


public class Popup_Settings : GameState
{
    [SerializeField] private Toggle vibrationToggle = null;
    [SerializeField] private Toggle soundToggle = null;
    [SerializeField] private Toggle musicToggle = null;
    [SerializeField] private Button ok = null;
    [SerializeField] private Button background = null;

    private void Start()
    {
        musicToggle.isOn = AudioManager.MusicVolume > 0;
        soundToggle.isOn = AudioManager.SoundVolume > 0;
        if (vibrationToggle)
            vibrationToggle.isOn = Settings.Vibration;

        musicToggle.onValueChanged.AddListener(on => AudioManager.MusicVolume = (on ? 100 : 0));
        soundToggle.onValueChanged.AddListener(on => AudioManager.SoundVolume = (on ? 100 : 0));
        if (vibrationToggle)
            vibrationToggle.onValueChanged.AddListener(on => Settings.Vibration = on);

        ok.onClick.AddListener(Back);
        background.onClick.AddListener(Back);

        UIShowHide.ShowAll(transform);
    }
}
