using System.Collections.Generic;
using EyePM;
using UnityEngine;
using UnityEngine.UI;


public class Popup_Credits : GameState
{
    [SerializeField] private List<Button> backButtons = new List<Button>(2);

    private void Start()
    {
        foreach (Button button in backButtons)
            button.onClick.AddListener(Back);
        UIShowHide.ShowAll(transform);
    }
}