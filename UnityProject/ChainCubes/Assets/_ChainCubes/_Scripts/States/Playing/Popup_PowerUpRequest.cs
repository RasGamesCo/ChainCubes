﻿using System;
using System.Collections.Generic;
using EyePM;
using GameAnalyticsSDK;
using Ras.RewardedAdShowButton;
using UnityEngine;
using UnityEngine.UI;


// ReSharper disable StringLiteralTypo


public class Popup_PowerUpRequest : GameState
{
    [SerializeField] private GameObject bomb = null;
    [SerializeField] private GameObject life = null;
    [SerializeField] private RewardAdShowButton getReward = null;
    [SerializeField] private Button no = null;
    [SerializeField] private Button backgroundBack = null;

    private const int RewardCount = 1;

    public void Setup(State_Playing.PowerUpType powerUpType, Action<int> onSuccess)
    {
        bomb.gameObject.SetActive(powerUpType == State_Playing.PowerUpType.Bomb);
        life.gameObject.SetActive(powerUpType == State_Playing.PowerUpType.Life);

        no.onClick.AddListener(Back);
        backgroundBack.onClick.AddListener(Back);

        getReward.MainButton.onClick.AddListener(() =>
        {
            getReward.SetState(State.Loading);
            RasAd.Show(RasConfig.AdPlaces.powerUpRequestRewarded, (completed, shown) =>
            {
                if (completed)
                {
                    AudioManager.PlaySound(RasFactory.Sounds.rewardClaimSuccess);
                    onSuccess.Invoke(RewardCount);
                    Back();
                }
                else if(shown)
                    getReward.SetState(State.Normal);
            });
        });

        Time.timeScale = 0;
        UIShowHide.ShowAll(transform);
    }

    public override void Back()
    {
        Time.timeScale = 1;
        base.Back();
    }
}
