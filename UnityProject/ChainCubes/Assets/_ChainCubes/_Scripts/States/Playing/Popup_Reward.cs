﻿using System;
using System.Collections.Generic;
using EyePM;
using Ras.RewardedAdShowButton;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class Popup_Reward : GameState
{
    [SerializeField] private List<Text> rewardValues = null;
    [SerializeField] private Image rewardImage = null;
    [SerializeField] private GameObject bombReward = null;
    [SerializeField] private GameObject lifeReward = null;
    [SerializeField] private RewardAdShowButton claim = null;
    [SerializeField] private GameObject encouragement = null;
    [SerializeField] private List<GameObject> toDeactivateOnNoReward = null;
    [SerializeField] private Text encouragementText = null;
    [SerializeField] private Button noThanks = null;
    [SerializeField] private Button greatBtn = null;

    private const int RewardCount = 1;
    private Action<int, State_Playing.PowerUpType> onGetReward = null;
    private int grabbedRewardCount = 0;
    private State_Playing.PowerUpType grabbedRewardType = State_Playing.PowerUpType.None;
    private bool rewardedAdUsed = false;

    public void Setup(long rewardValue, Action<int, State_Playing.PowerUpType> onGetReward)
    {
        foreach (Text text in rewardValues)
            text.text = BaseCube.RoundValues(rewardValue);

        RasFactory.BaseCubes.GetMaterial(rewardValue, out Color color);
        rewardImage.color = color;


        if (rewardValue < 2048)
        {
            grabbedRewardType = Random.Range(0f, 1f) <= RasConfig.Rewards.bombToLifePercent ? State_Playing.PowerUpType.Bomb : State_Playing.PowerUpType.Life;
            grabbedRewardCount = RewardCount;
            this.onGetReward = onGetReward;
        }
        else
        {
            grabbedRewardType = State_Playing.PowerUpType.None;
            grabbedRewardCount = 0;
            this.onGetReward = null;
        }

        encouragement.gameObject.SetActive(grabbedRewardType == State_Playing.PowerUpType.None);
        foreach (GameObject go in toDeactivateOnNoReward)
            go.SetActive(grabbedRewardType != State_Playing.PowerUpType.None);
        greatBtn.gameObject.SetActive(grabbedRewardType == State_Playing.PowerUpType.None);
        bombReward.gameObject.SetActive(grabbedRewardType == State_Playing.PowerUpType.Bomb);
        lifeReward.gameObject.SetActive(grabbedRewardType == State_Playing.PowerUpType.Life);
        if (grabbedRewardType == State_Playing.PowerUpType.None)
            encouragementText.text = GetEncouragementText(rewardValue);

        // Claim Buttons
        claim.MainButton.SetText("CLAIM x" + RasConfig.AdPlaces.newBigCubeRewardClaimRewarded.reward);
        claim.MainButton.onClick.AddListener(() =>
        {
            claim.SetState(State.Loading);
            RasAd.Show(RasConfig.AdPlaces.newBigCubeRewardClaimRewarded, (completed, shown) =>
            {
                if (completed)
                {
                    AudioManager.PlaySound(RasFactory.Sounds.rewardClaimSuccess);
                    grabbedRewardCount *= RasConfig.AdPlaces.newBigCubeRewardClaimRewarded.reward;
                    rewardedAdUsed = true;
                    Back();
                }
                else if (shown)
                    claim.SetState(State.Normal);
            });
        });

        noThanks.onClick.AddListener(Back);
        greatBtn.onClick.AddListener(Back);
        AudioManager.PlaySound(RasFactory.Sounds.newBigCubePopup);

        Time.timeScale = 0;
        UIShowHide.ShowAll(transform);
    }

    private string GetEncouragementText(long rewardValue)
    {
        int rand = Random.Range(0, 6);

        switch (rand)
        {
            case 0: return "Great Job\nCan you reach " + BaseCube.RoundValues(rewardValue * 2) + "?";
            case 1: return "You Don't give up, do you?\nYou will Never make " + BaseCube.RoundValues(rewardValue * 2) + ", though!";
            case 2: return "Fantastic!\nNow try to get " + BaseCube.RoundValues(rewardValue * 2) + "!";
            case 3: return "Wow, How about " + BaseCube.RoundValues(rewardValue * 2) + "?";
            case 4: return "hmm, that's something!\nShall we wait for " + BaseCube.RoundValues(rewardValue * 2) + "?";
            default: return "You are not giving, Are you?\nWhat about " + BaseCube.RoundValues(rewardValue * 2) + "?";
        }

    }

    public override void Back()
    {
        if (!rewardedAdUsed)
            RasAd.Show(RasConfig.AdPlaces.newBigCubeInterstitial);
        Time.timeScale = 1;
        base.Back();
        onGetReward?.Invoke(grabbedRewardCount, grabbedRewardType);
    }
}