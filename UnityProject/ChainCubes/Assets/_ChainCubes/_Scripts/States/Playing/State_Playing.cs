﻿using UnityEngine;
using UnityEngine.UI;
using EyePM;
using System;
using System.Collections.Generic;


public class State_Playing : GameState
{
    public enum PowerUpType
    {
        None,
        Bomb,
        Life
    }

    [Serializable]
    private class PowerUpPresenter
    {
        public PowerUpType type = PowerUpType.None;
        public Button useRequest = null;
        public Button increaseRequest = null;
        public Text count = null;
    }

    [SerializeField] private PlayerController controller = null;
    [Space]
    [SerializeField] private Button settings = null;
    // [SerializeField] private Button noAds = null;
    [SerializeField] private Text highScore = null;
    [SerializeField] private Text score = null;
    [SerializeField] private GameObject newHighScore = null;
    [SerializeField] private List<PowerUpPresenter> powerUpPresenters = new List<PowerUpPresenter>();
    [Space]
    [SerializeField] private RectTransform uiScoreParent = null;
    [SerializeField] private RectTransform scoreUiTextPrefab;
    [SerializeField] private TVStatePlayingFocusHandler tvStatePlayingFocusHandler = null;

    private PowerUpType activePowerUp = PowerUpType.None;
    private long pendingRewardValue = -1;
    private bool gameFinished = false;
    private bool isPointerDown = false;

    private void Start()
    {
        settings.onClick.AddListener(() => Game.Instance.OpenPopup<Popup_SettingsInGame>());
        // noAds.onClick.AddListener(() => Game.Instance.OpenPopup<Popup_NoAds>());

        foreach (PowerUpPresenter powerUpPresenter in powerUpPresenters)
        {
            powerUpPresenter.useRequest.onClick.AddListener(() => RequestUsePowerUp(powerUpPresenter.type));
            powerUpPresenter.increaseRequest.onClick.AddListener(() => RequestPowerUp(powerUpPresenter.type));
        }

        UpdatePowerUpTexts();
        UpdateScoreTexts();

        Playground.Instance.onBombCollided = BombCollided;
        Playground.Instance.OnCubeMerged = CubesMerged;
        Playground.Instance.onGameOver = GameLosing;
        UIShowHide.ShowAll(transform);
        AudioManager.PlaySound(RasFactory.Sounds.start);

        #if TV
        Destroy(controller.gameObject);
        #else
        controller.onPointerDown = () => { isPointerDown = true; };
        controller.onPointerUp = () =>
        {
            if (!isPointerDown)
                return;
            isPointerDown = false;
            Shoot();
        };
        #endif
        RasAd.Show(RasConfig.AdPlaces.mainBanner);
    }

    private void RequestUsePowerUp(PowerUpType powerUpType)
    {
        if (Profile.GetPowerUp(powerUpType) <= 0)
        {
            RequestPowerUp(powerUpType);
            return;
        }

        activePowerUp = powerUpType;
        Playground.Instance.UsePowerUp(powerUpType);
        switch (powerUpType)
        {
            case PowerUpType.Bomb:
                AudioManager.PlaySound(RasFactory.Sounds.useBomb);
                break;
            case PowerUpType.Life:
                AudioManager.PlaySound(RasFactory.Sounds.useLife);
                break;
            default: throw new ArgumentOutOfRangeException(nameof(powerUpType), powerUpType, null);
        }
    }

    private void RequestPowerUp(PowerUpType powerUpType)
    {
        Game.Instance.OpenPopup<Popup_PowerUpRequest>().Setup(powerUpType, amount => AddPowerUp(amount, powerUpType));
    }

    private void AddPowerUp(int amount, PowerUpType powerUpType)
    {
        Profile.EarnPowerUp(powerUpType, amount);
        UpdatePowerUpTexts();
    }

    private void ActivePowerUpUsed()
    {
        Profile.SpendPowerUp(activePowerUp, 1);
        UpdatePowerUpTexts();
        activePowerUp = PowerUpType.None;
    }

    private void UpdatePowerUpTexts()
    {
        foreach (PowerUpPresenter powerUpPresenter in powerUpPresenters)
            powerUpPresenter.count.text = Profile.GetPowerUp(powerUpPresenter.type).ToString();
    }

    private void UpdateScoreTexts()
    {
        score.text = PlayModel.stats.score.ToString();
        highScore.text = "Best: " + PlayModel.highScore;

        bool isNewHighScore = PlayModel.stats.score >= PlayModel.highScore;
        highScore.gameObject.SetActive(!isNewHighScore);
        newHighScore.SetActive(isNewHighScore && PlayModel.highScore != 0);
    }

    private void Update()
    {
        if (gameFinished)
            return;
        if (PlayModel.stats.activeBaseCube)
        {
            #if TV
            float moveH = 0;
            if (Game.Instance.CurrentPopup == null && Game.Instance.CurrentState is State_Playing && tvStatePlayingFocusHandler.State == TVStatePlayingFocusHandler.FocusState.FocusOnGame)
                moveH = Input.GetAxis("Horizontal") * RasConfig.Gameplay.activeBaseCube.moveSpeedTV;
            #else
            float moveH = controller.Value.x;
            #endif

            PlayModel.stats.activeBaseCube.Move(moveH);
            PlayModel.stats.activeBaseCube.LimitMoving();

            #if TV
            if (Game.Instance.CurrentPopup == null && Game.Instance.CurrentState is State_Playing && tvStatePlayingFocusHandler.State == TVStatePlayingFocusHandler.FocusState.FocusOnGame)
            {
                if (Input.GetButtonDown("Shoot"))
                    isPointerDown = true;
                if (isPointerDown && Input.GetButtonUp("Shoot"))
                {
                    isPointerDown = false;
                    Shoot();
                }
            }
            #endif
        }
    }

    private void Shoot()
    {
        if (!PlayModel.stats.activeBaseCube.Shoot())
            return;
        if (activePowerUp != PowerUpType.None)
            ActivePowerUpUsed();
        else
            PlayModel.stats.baseCubes.Add(PlayModel.stats.activeBaseCube.GetComponent<BaseCube>());
        Playground.Instance.CreateActiveCube();
        AudioManager.PlaySound(RasFactory.Sounds.shoot);
    }

    private void BombCollided(BaseCube baseCube)
    {
        AddScore(baseCube);
    }

    private void CubesMerged(BaseCube newCube)
    {
        PlayModel.stats.baseCubes.Add(newCube);
        AddScore(newCube);
        if (Settings.Vibration)
            Vibration.Vibrate(RasConfig.StaticData.vibration.cubeMerge);
    }

    private void AddScore(BaseCube cube)
    {
        PlayModel.stats.score += cube.Value;
        UpdateScoreTexts();
        ShowScoreUI(cube.transform.position, cube.Value);
        HandleGivingReward(cube.Value);
    }

    private void ShowScoreUI(Vector3 pos, long value)
    {
        Vector2 uiPos = uiScoreParent.WorldPointToLocalPoint(pos, null, Game.Instance.Canvas);
        RectTransform instantiated = Instantiate(scoreUiTextPrefab, uiScoreParent);
        instantiated.GetChild(0).GetComponent<Text>().text = "+ " + value;
        instantiated.anchoredPosition = uiPos;
    }

    private void HandleGivingReward(long newCubeValue)
    {
        bool shouldShow = newCubeValue >= 128 && ((newCubeValue != 128 && newCubeValue != 256) || (Profile.ShownRewards.Contains(newCubeValue) == false));
        if (!shouldShow)
        {
            AudioManager.PlaySound(RasFactory.Sounds.baseCubeMerge);
            return;
        }
        AudioManager.PlaySound(RasFactory.Sounds.bigBaseCubeMerge);

        pendingRewardValue = newCubeValue;
        CancelInvoke(nameof(ShowReward));
        Invoke(nameof(ShowReward), RasConfig.Rewards.delayToShowPopup);
    }

    private void ShowReward()
    {
        Profile.AddShownRewards(pendingRewardValue);
        if(!gameFinished)
            Game.Instance.OpenPopup<Popup_Reward>().Setup(pendingRewardValue, AddPowerUp);
        pendingRewardValue = -1;
        isPointerDown = false;
    }

    private void GameLosing()
    {
        if (gameFinished)
            return;
        gameFinished = true;
        PlayModel.onLoosing(loosed =>
        {
            if (loosed)
                GameLoosed();
            else
                PlayerRevived();
        });
    }

    private void PlayerRevived()
    {
        Transform reviveBox = RasFactory.ReviveBox.Create(Playground.Instance.transform);
        gameFinished = false;
        Playground.Instance.PlayerRevived();
    }

    private void GameLoosed()
    {
        PlayModel.onLosed(() => base.Back());
        RasAnalytics.Progression.Fail(PlayModel.stats.currentBestCubeValue);
    }

    public override void Back()
    {
        PlayModel.onLeaveRequest(null);
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
            PlayModel.onLeaveByClosingApplication?.Invoke();
    }
}
