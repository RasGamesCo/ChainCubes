﻿using EyePM;
using UnityEngine;
using UnityEngine.UI;


public class State_MainMenu : GameState
{
    [SerializeField] private Button creditsButton = null;
    [SerializeField] private Button settingsButton = null;
    // [SerializeField] private Button leaderBoardButton = null;
    // [SerializeField] private Button noAdsButton = null;
    [SerializeField] private Button playButton = null;

    private void Start()
    {
        UIShowHide.ShowAll(transform);

        creditsButton.onClick.AddListener(() => Game.Instance.OpenPopup<Popup_Credits>());
        settingsButton.onClick.AddListener(() => Game.Instance.OpenPopup<Popup_Settings>());
        // leaderBoardButton.onClick.AddListener(() => Game.Instance.OpenState<State_Leaderboard>());
        // noAdsButton.onClick.AddListener(() => Game.Instance.OpenState<Popup_NoAds>());
        playButton.onClick.AddListener(() => Game.Instance.OpenState<State_Main>());
    }

    public override void Back()
    {
        Game.Instance.OpenPopup<Popup_Exit>().Setup( yes =>
        {
            if (yes)
                Application.Quit();
        });
    }
}
