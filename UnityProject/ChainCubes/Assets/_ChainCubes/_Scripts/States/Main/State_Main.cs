﻿using EyePM;


public class State_Main : GameState
{
    private void Start()
    {
        CampaignLogic.ResetGame();
        Playground.Instance.Reset();

        if (Profile.IsPlaying)
            Playground.Instance.LoadFromProfile(LoadCompleted);
        else
            Playground.Instance.LoadFromScratch(LoadCompleted);

        UIShowHide.ShowAll(transform);
    }

    private void LoadCompleted()
    {
        StartGame();
    }

    private void StartGame()
    {
        CampaignLogic.StartGame();
    }

    public override void Back()
    {
    }
}
