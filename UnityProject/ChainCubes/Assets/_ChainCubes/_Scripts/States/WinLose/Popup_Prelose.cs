﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EyePM;
using Ras.RewardedAdShowButton;


public class Popup_Prelose : GameState
{
    [SerializeField] private Image bestCreatedCube = null;
    [SerializeField] private List<GameObject> onlyOnHighScore = new List<GameObject>();
    [SerializeField] private List<GameObject> onlyOnNotHighScore = new List<GameObject>();
    [SerializeField] private List<Text> cubeValues = new List<Text>();
    [SerializeField] private List<Text> scores = new List<Text>();
    [SerializeField] private Text highScores = null;
    [SerializeField] private RewardAdShowButton continueButton = null;
    [SerializeField] private Button noThankButton = null;

    private System.Action<bool> callbackFunc = null;

    private void Start()
    {
        if (Settings.Vibration)
            Vibration.Vibrate(RasConfig.StaticData.vibration.lose);
        Time.timeScale = 0;
        UIShowHide.ShowAll(transform);
    }


    public Popup_Prelose Setup(long score, long biggestCubeCreated, bool isHighScore, System.Action<bool> callback)
    {
        SetupVisuals(score, biggestCubeCreated, isHighScore);
        callbackFunc = callback;

        continueButton.MainButton.onClick.AddListener(() =>
        {
            continueButton.SetState(State.Loading);
            RasAd.Show(RasConfig.AdPlaces.reviveRewarded, (completed, shown) =>
            {
                if (completed)
                    Close(true);
                else
                    continueButton.SetState(State.Normal);
            });
        });

        noThankButton.onClick.AddListener(Back);

        return this;
    }

    private void SetupVisuals(long score, long biggestCubeCreated, bool isHighScore)
    {
        bestCreatedCube.color = RasFactory.BaseCubes.GetMaterial(biggestCubeCreated).color;

        foreach (GameObject go in onlyOnHighScore)
            go.SetActive(isHighScore);
        foreach (GameObject go in onlyOnNotHighScore)
            go.SetActive(!isHighScore);
        foreach (Text scoreText in scores)
            scoreText.text = "Score: "+ score;
        highScores.text = "Best HighScore: " + Profile.HighScore;
        foreach (Text cubeValue in cubeValues)
            cubeValue.text = BaseCube.RoundValues(biggestCubeCreated);
    }

    private void Close(bool keepMove)
    {
        base.Back();
        callbackFunc?.Invoke(keepMove);
        Time.timeScale = 1;
    }

    public override void Back()
    {
        Close(false);
        RasAd.Show(RasConfig.AdPlaces.loseInterstitial);
        Time.timeScale = 1;
    }
}
