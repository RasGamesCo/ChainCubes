﻿using UnityEngine;
using UnityEngine.UI;

namespace EyePM
{
    public class Popup_Exit : GameState
    {
        [SerializeField] private Button okButton = null;
        [SerializeField] private Button cancelButton = null;
        [SerializeField] private Button backgroundCancelButton = null;

        private System.Action<bool> callbackFunc = null;

        public Popup_Exit Setup(System.Action<bool> callback)
        {
            callbackFunc = callback;

            okButton.onClick.AddListener(() => Close(true));
            cancelButton.onClick.AddListener(() => Close(false));
            backgroundCancelButton.onClick.AddListener(() => Close(false));

            return this;
        }

        private void Start()
        {
            UIShowHide.ShowAll(transform);
        }

        public override void Back()
        {
            Close(false);
        }

        private void Close(bool isOk)
        {
            base.Back();
            callbackFunc?.Invoke(isOk);
        }
    }
}