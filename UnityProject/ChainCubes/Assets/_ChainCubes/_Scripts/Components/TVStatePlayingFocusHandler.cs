using System;
using EyePM;
using UnityEngine;
using UnityEngine.UI;


public class TVStatePlayingFocusHandler : MonoBehaviour
{
    public enum FocusState
    {
        FocusOnGame,
        FocusOnMenu
    }

    [SerializeField] private TVInteractableHandler tvInteractableHandler = null;
    [SerializeField] private Image dimEffect = null;
    [SerializeField] private GameObject tutorialGameFocus = null;
    [SerializeField] private GameObject tutorialMenuFocus = null;
    [Space]
    [SerializeField] private Button settingsButton = null;
    [SerializeField] private Button noAdsButton = null;
    [SerializeField] private Button lifeButton = null;
    [SerializeField] private Button bombButton = null;

    #if TV
    private FocusState state;
    public FocusState State
    {
        get => state;
        set
        {
            state = value;
            AdoptState();
        }
    }

    private void Awake()
    {
        State = FocusState.FocusOnGame;

        settingsButton.onClick.AddListener(() => State = FocusState.FocusOnGame);
        noAdsButton.onClick.AddListener(() => State = FocusState.FocusOnGame);
        lifeButton.onClick.AddListener(() => State = FocusState.FocusOnGame);
        bombButton.onClick.AddListener(() => State = FocusState.FocusOnGame);

        tvInteractableHandler.onGettingEnabled += AdoptState; // Yes, I know Myself!!! In Update function we May toggle state, and that will call adopt state, and adopt state May enable TVIntractableHandler and at this line we are causing ANOTHER EXTRA AdoptState CALL, but Dude, Thats ok to us
    }

    private void Update()
    {
        if (Input.GetButtonDown("ExitTV") && Game.Instance.CurrentPopup == null && Game.Instance.CurrentState is State_Playing)
            ToggleState();
    }

    private void ToggleState()
    {
        AudioManager.PlaySound(RasFactory.Sounds.buttonClick);
        State = State == FocusState.FocusOnGame ? FocusState.FocusOnMenu : FocusState.FocusOnGame;
    }

    private void AdoptState()
    {
        switch (State)
        {
            case FocusState.FocusOnGame:
                tvInteractableHandler.enabled = false;
                dimEffect.gameObject.SetActive(false);
                tutorialGameFocus.SetActive(true);
                tutorialMenuFocus.SetActive(false);
                break;
            case FocusState.FocusOnMenu:
                tvInteractableHandler.enabled = true;
                dimEffect.gameObject.SetActive(true);
                tutorialGameFocus.SetActive(false);
                tutorialMenuFocus.SetActive(true);
                break;
            default: throw new ArgumentOutOfRangeException(nameof(State), State, null);
        }
    }
    #endif
}