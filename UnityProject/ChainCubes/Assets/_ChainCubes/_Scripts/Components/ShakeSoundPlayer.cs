﻿using UnityEngine;
using EyePM;


public class ShakeSoundPlayer : MonoBehaviour
{
    //NOTE: This function is called from Unity Animation Event
    public void PlayeSound()
    {
        AudioManager.PlaySound(RasFactory.Sounds.shakePlayButton);
    }
}
