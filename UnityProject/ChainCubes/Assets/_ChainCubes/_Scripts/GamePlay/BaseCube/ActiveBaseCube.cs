﻿using System;
using UnityEngine;


public class ActiveBaseCube : MonoBehaviour
{
    private enum StateMode
    {
        None,
        GettingReady,
        Ready
    }


    private StateMode State { get; set; } = StateMode.None;
    private Rigidbody rigid = null;
    private new BoxCollider collider = null;
	private GameObject frontTrail = null;

    private void Awake()
    {
		frontTrail = RasFactory.BaseCubes.CreateTrail(transform).gameObject;
        rigid = GetComponent<Rigidbody>();
        collider = GetComponent<BoxCollider>();

        collider.material = RasFactory.BaseCubes.NormalPhysicMaterial;
        rigid.constraints = RigidbodyConstraints.None;

        ChangeState(StateMode.GettingReady);
    }

    private void ChangeState(StateMode stateMode)
    {
        if (State == stateMode)
            return;
        State = stateMode;
        switch (stateMode)
        {
            case StateMode.GettingReady:
                transform.localScale = Vector3.zero;
                collider.enabled = false;
                rigid.useGravity = false;
                rigid.isKinematic = true;
				frontTrail.SetActive(false);
                break;
            case StateMode.Ready:
                transform.localScale = Vector3.one;
                collider.enabled = true;
                rigid.useGravity = true;
                rigid.isKinematic = false;
				frontTrail.SetActive(true);
                break;
            default: throw new ArgumentOutOfRangeException(nameof(stateMode), stateMode, null);
        }
    }

    private void Update()
    {
        if (State == StateMode.Ready)
            return;
        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, Time.deltaTime * RasConfig.Gameplay.activeBaseCube.scaleSpeed);
        if (Mathf.Abs(transform.localScale.x - 1) <= 0.02f)
            ChangeState(StateMode.Ready);
    }

    public void LimitMoving()
    {
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, RasConfig.Gameplay.activeBaseCube.bound.x, RasConfig.Gameplay.activeBaseCube.bound.y);
        transform.position = pos;
    }

    public void Move(float moveH)
    {
        if (State == StateMode.GettingReady)
            return;
        Vector3 move = new Vector3(moveH * Time.deltaTime, 0, 0);
        transform.position += move;
    }

    public bool Shoot()
    {
        if (State == StateMode.GettingReady)
            return false;

        rigid.AddForce(transform.forward * RasConfig.Gameplay.activeBaseCube.shootForce, ForceMode.VelocityChange);
        Destroy(frontTrail.gameObject);
		Destroy(this);
        return true;
    }
}
