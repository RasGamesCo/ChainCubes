﻿using System;
using UnityEngine;


[RequireComponent(typeof(BaseCube))]
public class PreMadeBaseCube : MonoBehaviour
{
    public enum StateMode
    {
        None,
        GettingReady,
        Ready
    }

    [SerializeField] private int value = -1;

    public StateMode State { get; private set; } = StateMode.None;
    public int Value => value;

    private Rigidbody rigid = null;
    private new BoxCollider collider = null;
    private float stabilityDuration = 0;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        collider = GetComponent<BoxCollider>();
        ChangeState(StateMode.GettingReady);
    }

    private void ChangeState(StateMode stateMode)
    {
        if (State == stateMode)
            return;
        State = stateMode;
        switch (stateMode)
        {
            case StateMode.GettingReady:
                collider.material = RasFactory.BaseCubes.BouncyPhysicMaterial;
                rigid.constraints = (RigidbodyConstraints) 122; //  RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
                break;
            case StateMode.Ready:
                collider.material = RasFactory.BaseCubes.NormalPhysicMaterial;
                rigid.constraints = RigidbodyConstraints.None;
                rigid.velocity = Vector3.zero;
                Destroy(this);
                break;
            default: throw new ArgumentOutOfRangeException(nameof(stateMode), stateMode, null);
        }
    }

    private void FixedUpdate()
    {
        if (State == StateMode.GettingReady)
        {
            if (rigid.velocity.magnitude <= RasConfig.StaticData.baseCubes.maxStabilityVelocity)
                stabilityDuration += Time.fixedDeltaTime;
            else
                stabilityDuration = 0;
            if (stabilityDuration >= RasConfig.StaticData.baseCubes.minStabilityDuration)
                ChangeState(StateMode.Ready);
        }
    }

    #if UNITY_EDITOR
    private BaseCube baseCube = null;
    private BaseCube BaseCube
    {
        get
        {
            if (baseCube == null)
                baseCube = GetComponent<BaseCube>();
            return baseCube;
        }
    }

    private void OnValidate()
    {
        BaseCube.Setup(Value);
    }
    #endif
}
