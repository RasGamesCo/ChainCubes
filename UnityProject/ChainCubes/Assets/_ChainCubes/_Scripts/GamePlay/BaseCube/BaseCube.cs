﻿using System;
using System.Collections.Generic;
using UnityEngine;



public class BaseCube : MonoBehaviour
{
    [SerializeField] private MeshRenderer meshRenderer = null;
    [SerializeField] private List<TextMesh> texts = new List<TextMesh>(6);

    public Rigidbody Rigid { private set; get; } = null;

    public long Value { private set; get; }
    public Vector3 Velocity
    {
        get => Rigid.velocity;
        set => Rigid.velocity = value;
    }

    private void Awake()
    {
        Rigid = GetComponent<Rigidbody>();
    }

    public BaseCube Setup(long value)
    {
        Value = value;
        meshRenderer.sharedMaterial = RasFactory.BaseCubes.GetMaterial(value);

        string roundValue = RoundValues(value);
        int fontSize = RasConfig.StaticData.baseCubes.fontSize.GetFontSizeOf(roundValue);
        foreach (TextMesh text in texts)
        {
            text.text = roundValue;
            text.fontSize = fontSize;
        }

        return this;
    }

    private void OnDestroy()
    {
        PlayModel.stats.baseCubes.Remove(this);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == PhysicsLayer.BaseCube)
        {
            BaseCube baseCube = collision.gameObject.GetComponent<BaseCube>();
            if (baseCube.Value == Value)

                Playground.Instance.RequestToDestroy(baseCube, this);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == PhysicsLayer.Life)
        {
            Life life = collision.gameObject.GetComponent<Life>();
            Playground.Instance.RequestToDestroy(this, life);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == PhysicsLayer.Bomb)
        {
            Destroy(gameObject);
            Playground.Instance.BombCollided(Setup(Value));
        }
        else if(other.gameObject.layer == PhysicsLayer.ReviveBox && GetComponent<ActiveBaseCube>() == null)
            Destroy(gameObject);
    }

    public static string RoundValues(long cubeNumber)
    {
        if (cubeNumber > 4096 && cubeNumber < Mathf.Pow(10, 6))
        {
            long cubeShortNumber = cubeNumber / 1024;
            return cubeShortNumber + "K";
        }
        else if (cubeNumber > Mathf.Pow(10, 6) && cubeNumber < Mathf.Pow(10, 9))
        {
            long cubeShortNumber = cubeNumber / (1048576); // 1024 * 1024
            return cubeShortNumber + "M";
        }
        else if (cubeNumber > Mathf.Pow(10, 9))
        {
            long cubeShortNumber = cubeNumber / (1073741824); // 1024 * 1024 * 1024
            return cubeShortNumber + "G";
        }
        else
        {
            long cubeShortNumber = cubeNumber;
            return cubeShortNumber.ToString();
        }
    }
}