﻿using System;
using EyePM;
using UnityEngine;


public class PlayerController : UiPlayerControllerBase
{
    public Vector2 Value { get; private set; }
    public Action onPointerDown = null;
    public Action onPointerUp = null;

    private void Start()
    {
        onStateChanged += newState =>
        {
            if (newState == State.Down)
                onPointerDown?.Invoke();
            else if (newState == State.Up)
                onPointerUp?.Invoke();
        };
    }

    protected override void Update()
    {
        base.Update();

        if (state == State.Hold)
            Value = delta * (RasConfig.Gameplay.activeBaseCube.moveSpeedTouch * Time.deltaTime);
        else
            Value = Vector2.zero;
    }
}
