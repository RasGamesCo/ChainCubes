﻿using UnityEngine;


public static partial class PhysicsLayer
{
    public static readonly int BaseCube = LayerMask.NameToLayer("BaseCube");
    public static readonly int BoardEnd = LayerMask.NameToLayer("BoardEnd");
    public static readonly int GameOverZone = LayerMask.NameToLayer("GameOverZone");
    public static readonly int Bomb = LayerMask.NameToLayer("Bomb");
    public static readonly int Life  = LayerMask.NameToLayer("Life");
    public static readonly int ReviveBox = LayerMask.NameToLayer("ReviveBox");
}
