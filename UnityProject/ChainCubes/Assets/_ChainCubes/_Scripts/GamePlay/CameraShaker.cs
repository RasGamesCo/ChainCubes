﻿using System.Collections;
using UnityEngine;
using Camera = Ras.Config.Camera;


public class CameraShaker : MonoBehaviour
{
    private bool canShake = true;

    public void Shake(Camera.ShakeProfiles.Profile profile)
    {
        StopCoroutine(DoShake(profile));
        StartCoroutine(DoShake(profile));
    }

    private IEnumerator DoShake(Camera.ShakeProfiles.Profile profile)
    {
        canShake = false;
        yield return null;
        canShake = true;
        Vector3 originalPos = transform.localPosition;
        float elapsed = 0.0f;
        while (elapsed < profile.duration && canShake)
        {
            float x = Random.Range(-1f, 1f) * profile.magnitude.x;
            float y = Random.Range(-1f, 1f) * profile.magnitude.y;
            transform.localPosition = new Vector3(x + originalPos.x, y + originalPos.y, originalPos.z);
            elapsed += Time.deltaTime;
            yield return null;
        }
        transform.localPosition = originalPos;
    }

    //////////////////////////////////////////////////////
    /// STATIC MEMBERS
    //////////////////////////////////////////////////////
    private static CameraShaker instance = null;
    public static CameraShaker Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<CameraShaker>();
                if (instance == null)
                {
                    instance = new GameObject().AddComponent<CameraShaker>();
                    instance.gameObject.name = instance.GetType().Name;
                }
            }
            return instance;
        }
    }
}
