﻿using EyePM;
using UnityEngine;


public class Life : MonoBehaviour
{
    [SerializeField] private ParticleSystem explosionParticle = null;
    [SerializeField] private ParticleSystem idleParticle = null;

    private bool done = false;

    private void OnCollisionEnter(Collision other)
    {
        if (done)
            return;
        if (done || (other.gameObject.layer != PhysicsLayer.BaseCube && other.gameObject.layer != PhysicsLayer.BoardEnd))
            return;
        done = true;
        PlayParticle();
        Destroy(gameObject);
        if (other.gameObject.layer == PhysicsLayer.BaseCube)
            Act();
    }

    private void PlayParticle()
    {
        idleParticle.transform.parent = Playground.Instance.transform;
        explosionParticle.transform.parent = Playground.Instance.transform;
        idleParticle.Stop(true);
        explosionParticle.Play(true);
    }

    private void Act()
    {
        AudioManager.PlaySound(RasFactory.Sounds.lifeExplosion);
        if (Settings.Vibration)
            Vibration.Vibrate(RasConfig.StaticData.vibration.lifeExplosion);
    }
}
