﻿using System.Collections;
using UnityEngine;
using EyePM;
using UnityEngine.Serialization;
using Vibration = EyePM.Vibration;


public class Bomb : MonoBehaviour
{
    [SerializeField] private ParticleSystem particle = null;
    [SerializeField] private SphereCollider trigger;
    private bool bombCheck = true;

    private void Start()
    {
        trigger.radius = RasConfig.Gameplay.bomb.radius;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (bombCheck && other.gameObject.layer == PhysicsLayer.BaseCube || other.gameObject.layer == PhysicsLayer.BoardEnd)
        {
            trigger.enabled = true;
            bombCheck = false;

            StartCoroutine(Explode());
        }
    }

    private IEnumerator Explode()
    {
        particle.gameObject.SetActive(true);
        particle.Play(true);
        particle.transform.parent = Playground.Instance.transform;

        AudioManager.PlaySound(RasFactory.Sounds.bombExplosion);
        CameraShaker.Instance.Shake(RasConfig.StaticData.camera.shakeProfiles.bombExplosion);
        if (Settings.Vibration)
            Vibration.Vibrate(RasConfig.StaticData.vibration.bombExplosion);

        yield return new WaitForSecondsRealtime(Time.deltaTime);
        Destroy(gameObject);
    }
}
