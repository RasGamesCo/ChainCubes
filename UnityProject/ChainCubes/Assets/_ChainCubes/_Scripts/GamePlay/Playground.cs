﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Ras.Config;
using Ras.Profile;
using EyePM;
using UnityEngine;
using Random = UnityEngine.Random;


public class Playground : MonoBehaviour
{
    private bool createdFromScratch = false;
    private readonly List<BaseCube> toDestroy = new List<BaseCube>(2);

    public Action onGameOver = delegate { };
    public Action<BaseCube> onBombCollided = delegate { };
    public Action<BaseCube> OnCubeMerged { set; private get; } = null;
    public Action OnPlayerRevived = null;

    public void Reset()
    {
        createdFromScratch = false;
        transform.RemoveChildren();
    }

    public void RequestToDestroy(BaseCube cubeA, BaseCube cubeB)
    {
        if (toDestroy.Count == 0)
        {
            Vector3 mergedPos = (cubeA.transform.position + cubeB.transform.position) / 2;
            long mergedVal = cubeA.Value * 2;
            toDestroy.Add(cubeA);
            toDestroy.Add(cubeB);
            StartCoroutine(Destroying(() => InstantiateMergedCubes(mergedVal, mergedPos)));
        }
    }

    public void RequestToDestroy(BaseCube targetCube, Life life)
    {
        if (toDestroy.Count == 0)
        {
            Vector3 mergedPos = (targetCube.transform.position + life.transform.position) / 2;
            long mergedVal = targetCube.Value * 2;
            toDestroy.Add(targetCube);
            StartCoroutine(Destroying(() => InstantiateMergedCubes(mergedVal, mergedPos)));
        }
    }

    private IEnumerator Destroying(Action onComplete)
    {
        foreach (BaseCube cubeObjToDestroy in toDestroy)
            cubeObjToDestroy.Rigid.isKinematic = true;
        WaitForSeconds wait = new WaitForSeconds(Time.deltaTime * RasConfig.Gameplay.mergedCube.waitForMerged);
        yield return wait;

        foreach (BaseCube cubeObjToDestroy in toDestroy)
        {
            if (cubeObjToDestroy) // Be cause it can be destroyed with bomb
                Destroy(cubeObjToDestroy.gameObject);
            yield return new WaitForSeconds(Time.deltaTime); // Because destroying in unity takes one frame long.
        }

        toDestroy.Clear();
        onComplete.Invoke();
    }

    private void InstantiateMergedCubes(long cubeVal, Vector3 pos)
    {
        BaseCube mergedCube = RasFactory.BaseCubes.Create(transform).Setup(cubeVal);
        mergedCube.transform.position = pos;
        Rigidbody rb = mergedCube.Rigid;
        Vector3 mergedForce = CalculateMergedForce(cubeVal, mergedCube);
        rb.AddForce(mergedForce);
        rb.angularVelocity = Random.insideUnitSphere * RasConfig.Gameplay.mergedCube.angularForce;

        RasFactory.BaseCubes.CreateMergeParticle(transform, mergedCube.transform.position + new Vector3(0,0.5f,0));

        OnCubeMerged.Invoke(mergedCube);

        if (cubeVal > PlayModel.stats.currentBestCubeValue)
        {
            PlayModel.stats.currentBestCubeValue = cubeVal;
            RasAnalytics.Progression.Start(cubeVal);
        }
    }

    private Vector3 CalculateMergedForce(long val, Component mergedCube)
    {
        Gameplay.MergedCube config = RasConfig.Gameplay.mergedCube;
        Vector3 force;

        BaseCube cubeSameValue = PlayModel.stats.baseCubes.Find(cube => (cube.Value == val));
        if (cubeSameValue)
            // TODO @Atieh -Feather: DO not force cube into camera ( game over line )
            force = (cubeSameValue.transform.position - mergedCube.transform.position) * RasConfig.Gameplay.mergedCube.forceToTargetCube;
        else
        {
            force = Random.onUnitSphere * Random.Range(config.forceRandomMin, config.forceRandomMax);
            force.z = Mathf.Abs(force.z);
        }

        force.y = Random.Range(config.forceYMin, config.forceYMax);
        return force;
    }

    public void LoadFromProfile(Action onComplete)
    {
        RasFactory.Boards.CreateEmpty(transform);

        foreach (Data.BaseCubeProfileData baseCubeProfileData in Profile.BaseCubes)
        {
            var cube = RasFactory.BaseCubes.Create(transform).Setup(baseCubeProfileData.value);
            cube.Velocity = baseCubeProfileData.velocity;
            cube.transform.position = baseCubeProfileData.position + new Vector3(0, 0.01f, 0); // for safety of Physic reactions
            cube.transform.eulerAngles = baseCubeProfileData.rotation;

            PlayModel.stats.baseCubes.Add(cube);
        }
        CreateActiveCube(Profile.ActiveBaseCubeValue);
        onComplete?.Invoke();
    }

    public void LoadFromScratch(Action onComplete)
    {
        if (createdFromScratch)
            return;
        createdFromScratch = true;

        Transform board = RasFactory.Boards.CreateRandomPreMade(transform);
        var preMadeBaseCubes = board.GetComponentsInChildren<PreMadeBaseCube>().ToList();
        PlayModel.stats.baseCubes = new List<BaseCube>();
        foreach (PreMadeBaseCube preMadeBaseCube in preMadeBaseCubes)
        {
            var baseCube = preMadeBaseCube.GetComponent<BaseCube>().Setup(preMadeBaseCube.Value);
            PlayModel.stats.baseCubes.Add(baseCube);
        }

        Time.timeScale = RasConfig.Gameplay.loadFromScratchTimeScale;
        StartCoroutine(CheckForPreMadeBaseCubesToGetReady(preMadeBaseCubes, () =>
        {
            CreateActiveCube();
            Time.timeScale = 1;
            onComplete.Invoke();
        }));
    }

    private IEnumerator CheckForPreMadeBaseCubesToGetReady(List<PreMadeBaseCube> preMadeBaseCubes, Action onComplete)
    {
        yield return null;

        bool allStopped;
        do
        {
            yield return null;
            allStopped = preMadeBaseCubes.All(preMadeBaseCube => !preMadeBaseCube || preMadeBaseCube.State != PreMadeBaseCube.StateMode.GettingReady);
        } while (!allStopped);
        onComplete.Invoke();
    }

    public void CreateActiveCube(long preSavedValue = -1)
    {
        long cubeValue = preSavedValue == -1 ? GenerateRandomPossibleValue() : preSavedValue;
        BaseCube activeCube = RasFactory.BaseCubes.Create(transform).Setup(cubeValue);
        PlayModel.stats.activeBaseCube = activeCube.gameObject.AddComponent<ActiveBaseCube>();
    }

    private int GenerateRandomPossibleValue()
    {
        if (PlayModel.stats.baseCubes.Count == 0)
            return (int) Mathf.Pow(2, Random.Range(1, 6));
        var ordered = PlayModel.stats.baseCubes.OrderBy(x => x.Value).ToList();
        ordered.Reverse();
        float maxValueLog = Mathf.Log(ordered[0].Value, 2);
        int randomPower = ((int) Random.Range(1, maxValueLog));
        int power = (randomPower < 1) ? (1) : ((randomPower >= 6) ? (6) : (randomPower));

        return (int) Math.Pow(2, power);
    }

    public void UsePowerUp(State_Playing.PowerUpType type)
    {
        Transform powerUp = RasFactory.PowerUp.Create(transform, type);
        if (PlayModel.stats.activeBaseCube)
            Destroy(PlayModel.stats.activeBaseCube.gameObject);
        PlayModel.stats.activeBaseCube = powerUp.gameObject.AddComponent<ActiveBaseCube>();
    }

    public void GameLosing()
    {
        onGameOver.Invoke();
    }

    public void BombCollided(BaseCube baseCube)
    {
        onBombCollided.Invoke(baseCube);
    }

    public void PlayerRevived()
    {
        OnPlayerRevived?.Invoke();
    }

    //////////////////////////////////////////////////////
    /// STATIC MEMBERS
    //////////////////////////////////////////////////////
    private static Playground instance = null;
    public static Playground Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Playground>();
                if (instance == null)
                {
                    instance = new GameObject().AddComponent<Playground>();
                    instance.gameObject.name = instance.GetType().Name;
                }
            }
            return instance;
        }
    }
}
