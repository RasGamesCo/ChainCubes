
using System;
using EyePM;
using UnityEngine;


public class ReviveBox : MonoBehaviour
{
    private void Awake()
    {
        gameObject.layer = PhysicsLayer.ReviveBox;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == PhysicsLayer.BaseCube && other.gameObject.GetComponent<ActiveBaseCube>() == null)
            Destroy(gameObject);
    }
}
