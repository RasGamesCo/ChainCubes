﻿using UnityEngine;


public class GameOverZone : MonoBehaviour
{
    private float timer = 0;
    private bool done = false;

    private void Awake()
    {
        Playground.Instance.OnPlayerRevived = ResetIt;
        gameObject.layer = PhysicsLayer.GameOverZone;
        ResetIt();
    }

    public void ResetIt()
    {
        done = false;
        timer = 0;
        enabled = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (done || other.gameObject.layer != PhysicsLayer.BaseCube)
            return;
        timer += Time.deltaTime;
        if (timer > RasConfig.Gameplay.checkGameOverTime)
        {
            Playground.Instance.GameLosing();
            done = true;
            enabled = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        timer = 0;
    }
}
