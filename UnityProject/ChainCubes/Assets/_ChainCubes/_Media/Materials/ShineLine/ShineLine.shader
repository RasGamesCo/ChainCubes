﻿Shader "EyePM/UI/ShineLine"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _MainTex("Texture", 2D) = "white" {}
        _ShineTex("Shine Texture", 2D) = "black" {}
        _Speed("Speed", Float) = 5

        [Enum(Zero,0,One,1,DstColor,2,SrcColor,3,SrcAlpha,5,DstAlpha,7,OneMinusSrcAlpha,10)] _BlendSrc("SrcFactor", Int) = 5
        [Enum(Zero,0,One,1,DstColor,2,SrcColor,3,SrcAlpha,5,DstAlpha,7,OneMinusSrcAlpha,10)] _BlendDest("DstFactor", Int) = 10

        _StencilComp("Stencil Comparison", Float) = 8
        _Stencil("Stencil ID", Float) = 0
        _StencilOp("Stencil Operation", Float) = 0
        _StencilWriteMask("Stencil Write Mask", Float) = 255
        _StencilReadMask("Stencil Read Mask", Float) = 255

        _ColorMask("Color Mask", Float) = 15
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
        }

        Stencil
        {
            Ref[_Stencil]
            Comp[_StencilComp]
            Pass[_StencilOp]
            ReadMask[_StencilReadMask]
            WriteMask[_StencilWriteMask]
        }

        Cull Off
        ZWrite Off
        Blend[_BlendSrc][_BlendDest]

        Lighting Off
        ZTest[unity_GUIZTestMode]
        ColorMask[_ColorMask]

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 col : COLOR0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 col : COLOR0;
            };

            float4 _Color;
                float _Speed;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _ShineTex;
            float4 _ShineTex_ST;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv0 = TRANSFORM_TEX(v.uv, _MainTex);
                o.col = v.col * _Color;
                half2 uv = TRANSFORM_TEX(o.vertex, _ShineTex);
                    uv.x -= _Time.x * _Speed;

                half sint, cost;
                sincos(0.3f, sint, cost);
                o.uv1.x = 0.5f + (uv.x * cost - uv.y * sint);
                o.uv1.y = 0.5f + (-uv.x * sint - uv.y * cost);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv0);
                fixed4 shn = tex2D(_ShineTex, i.uv1);
                col.rgb += shn.a * (int)(i.col.a + 0.05f);
                return col * i.col;
            }
            ENDCG
        }
    }
}
